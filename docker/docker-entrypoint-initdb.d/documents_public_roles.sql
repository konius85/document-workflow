CREATE TABLE public.roles
(
    roleid integer DEFAULT nextval('roles_roleid_seq'::regclass) PRIMARY KEY NOT NULL,
    name__ varchar(255) NOT NULL
);
CREATE UNIQUE INDEX roles_roleid_uindex ON public.roles (roleid);
INSERT INTO public.roles (roleid, roleName) VALUES (1, 'Admin');
INSERT INTO public.roles (roleid, roleName) VALUES (2, 'User');