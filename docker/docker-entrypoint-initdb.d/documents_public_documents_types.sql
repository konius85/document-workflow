CREATE TABLE public.documents_types
(
    doctyp integer DEFAULT nextval('documents_types_doctyp_seq'::regclass) PRIMARY KEY NOT NULL,
    name__ varchar(255)
);
INSERT INTO public.documents_types (doctyp, name__) VALUES (1, 'Document');