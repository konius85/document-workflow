CREATE TABLE public.tasks
(
    taskid integer DEFAULT nextval('tasks_taskid_seq'::regclass) PRIMARY KEY NOT NULL,
    name__ varchar(255),
    descrp text,
    start_ timestamp DEFAULT now(),
    end___ timestamp DEFAULT now(),
    creatd timestamp DEFAULT now(),
    lastup timestamp DEFAULT now(),
    addusr integer,
    status integer,
    modusr integer,
    CONSTRAINT tasks_addusr_fkey FOREIGN KEY (addusr) REFERENCES public.users (usrId),
    CONSTRAINT tasks_modusr_fkey FOREIGN KEY (modusr) REFERENCES public.users (usrId)
);