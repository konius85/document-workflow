CREATE TABLE public.documents
(
    doc_id integer DEFAULT nextval('documents_doc_id_seq'::regclass) PRIMARY KEY NOT NULL,
    name__ varchar(255),
    creatd timestamp DEFAULT now(),
    lastup timestamp DEFAULT now(),
    addusr integer,
    status integer,
    type__ integer,
    modusr integer,
    CONSTRAINT documents_addusr_fkey FOREIGN KEY (addusr) REFERENCES public.users (usrId),
    CONSTRAINT documents_status_statid_fk FOREIGN KEY (status) REFERENCES public.status (statusId),
    CONSTRAINT documents_type___fkey FOREIGN KEY (type__) REFERENCES public.documents_types (documenttypeid),
    CONSTRAINT documents_modusr_fkey FOREIGN KEY (modusr) REFERENCES public.users (usrId)
);
COMMENT ON COLUMN public.documents.documentId IS 'Identyfikator dokumentu';
COMMENT ON COLUMN public.documents.name IS 'Nazwa dokumentu';
COMMENT ON COLUMN public.documents.createdDate IS 'Data utworzenia';
COMMENT ON COLUMN public.documents.lastUpdate IS 'Ostatnia aktualizacja';
COMMENT ON COLUMN public.documents.addUser IS 'Identyfikator użytkownika który dodał dokument';
COMMENT ON COLUMN public.documents.statusId IS 'Status dokumentu';
COMMENT ON COLUMN public.documents.documentType IS 'Typ dokumentu';
COMMENT ON COLUMN public.documents.modyfiactionUser IS 'Identyfikator użytkownika który ostatnio zmienił dokument';
INSERT INTO public.documents (documentId, name, createdDate, lastUpdate, addUser, statusId, documentType, modyfiactionUser) VALUES (1, 'test doc', '2018-11-20 20:47:24.349529', '2018-11-20 20:47:24.349529', 1, 1, 1, 1);
INSERT INTO public.documents (documentId, name, createdDate, lastUpdate, addUser, statusId, documentType, modyfiactionUser) VALUES (2, 'test 2', '2018-11-20 20:47:49.241912', '2018-11-20 20:47:49.241912', 1, 1, 1, 1);
INSERT INTO public.documents (documentId, name, createdDate, lastUpdate, addUser, statusId, documentType, modyfiactionUser) VALUES (3, 'DŁugi tekst w dokumencie', '2018-11-20 21:07:19.564443', '2018-11-20 21:07:19.564443', 1, 1, 1, 1);