CREATE TABLE public.users
(
    usr_id integer DEFAULT nextval('users_usr_id_seq'::regclass) PRIMARY KEY NOT NULL,
    fname_ varchar(255),
    creatd timestamp DEFAULT now(),
    lastup timestamp DEFAULT now(),
    passwd varchar(255) NOT NULL,
    lname_ varchar(255),
    role__ integer DEFAULT 2 NOT NULL,
    CONSTRAINT users_roles_roleid_fk FOREIGN KEY (usr_id) REFERENCES public.roles (roleid)
);
COMMENT ON COLUMN public.users.usrId IS 'Identyfikator użytkownika';
COMMENT ON COLUMN public.users.firstName IS 'Nazwa użytkownika';
COMMENT ON COLUMN public.users.createdDate IS 'Data utworzenia konta';
COMMENT ON COLUMN public.users.lastUpdate IS 'Ostatnia aktualizacja';
INSERT INTO public.users (usrId, firstName, createdDate, lastUpdate, password, lastName, roleId) VALUES (1, 'Paweł', '2018-11-21 17:45:58.867919', '2018-11-21 17:45:58.867919', 'wrerwerwerwfsfs', 'Konik', 1);