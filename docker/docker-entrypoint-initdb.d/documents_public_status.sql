CREATE TABLE public.status
(
    statid integer DEFAULT nextval('status_statid_seq'::regclass) PRIMARY KEY NOT NULL,
    typeid integer NOT NULL,
    doctyp varchar(255),
    name__ varchar(255)
);
COMMENT ON COLUMN public.status.statusId IS 'Identyfikator statusu
';
COMMENT ON COLUMN public.status.typeid IS 'Typ statusu';
COMMENT ON COLUMN public.status.documentTyp IS 'Typ dokumentu ''DOCUMENTS'', ''TASKS''';
COMMENT ON COLUMN public.status.name IS 'Nazwa statusu';
INSERT INTO public.status (statusId, typeid, documentTyp, name) VALUES (1, 1, '1', 'NOWY');