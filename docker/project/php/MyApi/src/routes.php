<?php

use Slim\Http\Request;
use Slim\Http\Response;

$app->group('/v1', function () {

    $this->group('/documents', function () {
        $this->map(['GET'], '', 'DocumentsController:getDocuments');
        $this->map(['GET'], '/documentsTypes', 'DocumentsController:getDocumentsTypes');
        $this->map(['POST'], '/add', 'DocumentsController:addDocument');
        $this->map(['POST'], '/update', 'DocumentsController:updateDocument');
        $this->map(['POST'], '/removeDocument', 'DocumentsController:removeDocument');
        $this->map(['GET'], '/{id}', 'DocumentsController:getDocumentById');
        $this->map(['GET'], '/name/{name}', 'DocumentsController:getDocumentByName');
    });

    $this->group('/status', function () {
        $this->map(['GET'], '', 'StatusController:getStatus');
    });

    $this->group('/upload', function () {
        $this->map(['POST'], '', 'FileController:uploadFile');
    });

    $this->group('/users', function () {
        $this->map(['GET'], '', 'UsersController:getUsers');
        $this->map(['GET'], '/{id}', 'UsersController:getUserById');
        $this->map(['POST'], '/authenticate', 'UsersController:authenticate');
        $this->map(['POST'], '/update', 'UsersController:update');
    });

    $this->group('/tasks', function () {
        $this->map(['GET'], '', 'TasksController:getTasks');
        $this->map(['GET'], '/today', 'TasksController:getTodayTask');
        $this->map(['POST'], '/update', 'TasksController:update');
        $this->map(['POST'], '/add', 'TasksController:addTask');
        $this->map(['GET'], '/day', 'TasksController:getTaskForDay');
        $this->map(['GET'], '/{id}', 'TasksController:getTaskById');
        $this->map(['GET'], '/document/{docId}', 'TasksController:getTaskForDocument');
        $this->map(['GET'], '/remove/{taskId}', 'TasksController:remove');
    });

})->add(new Auth());



