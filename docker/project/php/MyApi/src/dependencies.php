<?php
// DIC configuration

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

//
$container['db'] = function ($c) {
    $settings = $c->get('settings')['db'];
    $pdo = new PDO('pgsql:host='.$settings['host'].';port=5432;dbname='.$settings['dbname'], $settings['user'], $settings['pass']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    return $pdo;
};

$container['uploadPath'] = function ($c) {
    $path = __DIR__ . '/../app/upload/';
    $today = getdate();
    $shortDate = $today['year'] . '-' . $today['mon'] . '-' . $today['mday'];
    return array('fullPath' => $path . '' . $shortDate,
        'shortPath' => $shortDate);
};


$container['DocumentsController'] = function($c) use ($app) {
    return new DocumentsController( $app );
};

$container['StatusController'] = function($c) use ($app) {
    return new StatusController( $app );
};

$container['FileController'] = function($c) use ($app) {
    return new FileController( $app );
};

$container['UsersController'] = function($c) use ($app) {
    return new UsersController( $app );
};

$container['TasksController'] = function($c) use ($app) {
    return new TasksController( $app );
};

