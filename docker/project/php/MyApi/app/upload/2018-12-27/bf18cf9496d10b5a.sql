create table documents
(
  documentid         serial                  not null
    constraint documents_pkey
      primary key,
  name               varchar(255),
  createddate        timestamp default now(),
  lastupdate         timestamp default now(),
  adduserid          integer
    constraint documents_addusr_fkey
      references users,
  statusid           integer,
  documenttypeid     integer
    constraint documents_type___fkey
      references documents_types,
  modificationuserid integer
    constraint documents_modusr_fkey
      references users,
  description        text,
  isdeleted          boolean   default false not null,
  fileid             integer
);

comment on column documents.documentid is 'Identyfikator dokumentu';

comment on column documents.name is 'Nazwa dokumentu';

comment on column documents.createddate is 'Data utworzenia';

comment on column documents.lastupdate is 'Ostatnia aktualizacja';

comment on column documents.adduserid is 'Identyfikator użytkownika który dodał dokument';

comment on column documents.statusid is 'Status dokumentu';

comment on column documents.documenttypeid is 'Typ dokumentu';

comment on column documents.modificationuserid is 'Identyfikator użytkownika który ostatnio zmienił dokument';

comment on column documents.description is 'Opis dokumentu';

alter table documents
  owner to postgres;

INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (54, 'test', '2018-12-11 19:11:43.823098', '2018-12-11 19:11:43.823098', 1, 1, 1, null, 'tesDEST', false, 26);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (5, 'API', '2018-11-24 15:53:54.114348', '2018-11-24 15:53:54.114348', 1, 1, 1, null, null, false, 1);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (4, 'test doc', '2018-11-24 15:50:14.529318', '2018-11-24 15:50:14.529318', 1, 1, 1, null, null, false, 1);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (7, 'API', '2018-11-24 15:56:17.625585', '2018-11-24 15:56:17.625585', 1, 1, 1, null, null, false, 1);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (6, 'API', '2018-11-24 15:55:41.213707', '2018-11-24 15:55:41.213707', 1, 1, 1, null, null, false, 1);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (1, 'test doc', '2018-11-20 20:47:24.349529', '2018-11-20 20:47:24.349529', 1, 1, 1, 1, null, false, 1);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (3, 'DŁugi tekst w dokumencie', '2018-11-20 21:07:19.564443', '2018-11-20 21:07:19.564443', 1, 1, 1, 1, null, false, 1);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (2, 'test 2', '2018-11-20 20:47:49.241912', '2018-11-20 20:47:49.241912', 1, 1, 1, 1, null, false, 1);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (13, 'API', '2018-11-24 16:04:03.658085', '2018-11-24 16:04:03.658085', 1, 1, 1, null, null, false, 1);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (12, 'API', '2018-11-24 16:00:38.233929', '2018-11-24 16:00:38.233929', 1, 1, 1, null, null, false, 1);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (15, 'name', '2018-11-24 16:08:22.899396', '2018-11-24 16:08:22.899396', 1, 1, 1, null, null, false, 1);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (14, 'API', '2018-11-24 16:04:26.996834', '2018-11-24 16:04:26.996834', 1, 1, 1, null, null, false, 1);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (9, 'API', '2018-11-24 15:59:23.257016', '2018-11-24 15:59:23.257016', 1, 1, 1, null, null, false, 1);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (8, 'API', '2018-11-24 15:59:15.964590', '2018-11-24 15:59:15.964590', 1, 1, 1, null, null, false, 1);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (11, 'API', '2018-11-24 16:00:29.249421', '2018-11-24 16:00:29.249421', 1, 1, 1, null, null, false, 1);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (10, 'API', '2018-11-24 16:00:12.607096', '2018-11-24 16:00:12.607096', 1, 1, 1, null, null, false, 1);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (21, '', '2018-11-24 16:36:26.611615', '2018-11-24 16:36:26.611615', 1, 1, 1, null, null, false, 1);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (20, '', '2018-11-24 16:35:57.086072', '2018-11-24 16:35:57.086072', 1, 1, 1, null, null, false, 1);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (23, '', '2018-11-24 16:37:29.974179', '2018-11-24 16:37:29.974179', 1, 1, 1, null, null, false, 1);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (22, '', '2018-11-24 16:37:00.145313', '2018-11-24 16:37:00.145313', 1, 1, 1, null, null, false, 1);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (17, '', '2018-11-24 16:32:39.994138', '2018-11-24 16:32:39.994138', 1, 1, 1, null, null, false, 1);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (16, '', '2018-11-24 16:31:35.226446', '2018-11-24 16:31:35.226446', 1, 1, 1, null, null, false, 1);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (19, '', '2018-11-24 16:35:11.411323', '2018-11-24 16:35:11.411323', 1, 1, 1, null, null, false, 1);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (18, '', '2018-11-24 16:33:24.921838', '2018-11-24 16:33:24.921838', 1, 1, 1, null, null, false, 1);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (29, 'Web Storm', '2018-11-24 16:41:03.928557', '2018-11-24 16:41:03.928557', 1, 1, 1, null, null, false, 1);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (28, '123', '2018-11-24 16:40:43.046855', '2018-11-24 16:40:43.046855', 1, 1, 1, null, null, false, 1);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (30, 'asd', '2018-11-24 20:49:34.336208', '2018-11-24 20:49:34.336208', 1, 1, 1, null, null, false, 1);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (25, 'asd', '2018-11-24 16:39:03.475804', '2018-11-24 16:39:03.475804', 1, 1, 1, null, null, false, 1);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (24, '', '2018-11-24 16:38:21.839839', '2018-11-24 16:38:21.839839', 1, 1, 1, null, null, false, 1);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (27, '', '2018-11-24 16:40:14.305155', '2018-11-24 16:40:14.305155', 1, 1, 1, null, null, false, 1);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (26, '', '2018-11-24 16:39:39.897440', '2018-11-24 16:39:39.897440', 1, 1, 1, null, null, false, 1);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (55, 'test', '2018-12-11 19:15:25.960424', '2018-12-11 19:15:25.960424', 1, 1, 1, null, 'tesDEST', false, 27);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (57, 'test', '2018-12-11 19:16:58.511812', '2018-12-11 19:16:58.511812', 1, 1, 1, null, 'tesDEST', false, 28);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (59, 'test', '2018-12-11 19:17:03.495651', '2018-12-11 19:17:03.495651', 1, 1, 1, null, 'tesDEST', false, 29);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (89, 'test', '2018-12-11 19:19:23.874773', '2018-12-11 19:19:23.874773', 1, 1, 1, null, 'tesDEST', false, 30);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (96, 'test', '2018-12-11 19:38:39.869432', '2018-12-11 19:38:39.869432', 1, 1, 1, null, 'tesDEST', false, 31);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (99, 'test', '2018-12-11 19:40:54.012780', '2018-12-11 19:40:54.012780', 1, 1, 1, null, 'tesDEST', false, 32);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (101, 'test', '2018-12-11 19:42:58.337765', '2018-12-11 19:42:58.337765', 1, 1, 1, null, 'tesDEST', false, 33);
INSERT INTO public.documents (documentid, name, createddate, lastupdate, adduserid, statusid, documenttypeid, modificationuserid, description, isdeleted, fileid) VALUES (103, 'test', '2018-12-11 19:45:35.190867', '2018-12-11 19:45:35.190867', 1, 1, 1, null, 'tesDEST', false, 34);
create table documents_types
(
  documenttypeid serial not null
    constraint documents_types_pkey
      primary key,
  name           varchar(255)
);

alter table documents_types
  owner to documents;

INSERT INTO public.documents_types (documenttypeid, name) VALUES (1, 'Dokument');
INSERT INTO public.documents_types (documenttypeid, name) VALUES (2, 'Faktura');
INSERT INTO public.documents_types (documenttypeid, name) VALUES (3, 'Pismo');
INSERT INTO public.documents_types (documenttypeid, name) VALUES (4, 'Umowa');
create table files
(
  fileid      serial                  not null
    constraint files_pk
      primary key,
  name        text                    not null,
  filepath    text                    not null,
  adduser     integer,
  isdeleted   boolean   default false,
  createdate  timestamp default now() not null,
  encodedname varchar(255),
  documentid  integer,
  extension   char(100),
  mimetype    varchar(255)
);

alter table files
  owner to documents;

create unique index files_fileid_uindex
  on files (fileid);

INSERT INTO public.files (fileid, name, filepath, adduser, isdeleted, createdate, encodedname, documentid, extension, mimetype) VALUES (2, '1', '1', 1, false, '2018-12-10 20:17:43.111847', null, null, null, null);
INSERT INTO public.files (fileid, name, filepath, adduser, isdeleted, createdate, encodedname, documentid, extension, mimetype) VALUES (3, '. 82fadb42c1487f5b.pdf .', '. /var/www/html/MyApi/app/v1/Controllers/../../upload/2018-12-10 .', 1, false, '2018-12-10 20:19:17.393707', null, null, null, null);
INSERT INTO public.files (fileid, name, filepath, adduser, isdeleted, createdate, encodedname, documentid, extension, mimetype) VALUES (4, '1e4879be899af4eb.pdf', '/var/www/html/MyApi/app/v1/Controllers/../../upload/2018-12-10', 1, false, '2018-12-10 20:20:17.251633', null, null, null, null);
INSERT INTO public.files (fileid, name, filepath, adduser, isdeleted, createdate, encodedname, documentid, extension, mimetype) VALUES (5, '()', '$DIR_PATH$2018-12-10', 1, false, '2018-12-10 20:58:45.886228', '44ab14443a49186b.pdf', null, null, null);
INSERT INTO public.files (fileid, name, filepath, adduser, isdeleted, createdate, encodedname, documentid, extension, mimetype) VALUES (6, '()', '$DIR_PATH$2018-12-10', 1, false, '2018-12-10 20:59:54.663261', 'e03465269fd7d710.pdf', null, null, null);
INSERT INTO public.files (fileid, name, filepath, adduser, isdeleted, createdate, encodedname, documentid, extension, mimetype) VALUES (7, ' 2018.11.08_list_MEN_GIS_rodzice.pdf', '$DIR_PATH$2018-12-10', 1, false, '2018-12-10 21:01:52.391916', '58d7a1badefa59de.pdf', null, null, null);
INSERT INTO public.files (fileid, name, filepath, adduser, isdeleted, createdate, encodedname, documentid, extension, mimetype) VALUES (8, ' 2018.11.08_list_MEN_GIS_rodzice.pdf', '$DIR_PATH$2018-12-11', 1, false, '2018-12-11 18:34:34.776314', 'a4750a8abd36b422.pdf', 1, null, null);
INSERT INTO public.files (fileid, name, filepath, adduser, isdeleted, createdate, encodedname, documentid, extension, mimetype) VALUES (9, ' 2018.11.08_list_MEN_GIS_rodzice.pdf', '$DIR_PATH$2018-12-11', 1, false, '2018-12-11 18:36:23.741180', 'b7db43d264416eac.pdf', 1, null, null);
INSERT INTO public.files (fileid, name, filepath, adduser, isdeleted, createdate, encodedname, documentid, extension, mimetype) VALUES (10, ' 2018.11.08_list_MEN_GIS_rodzice.pdf', '$DIR_PATH$2018-12-11', 1, false, '2018-12-11 18:37:59.723610', '41dc105049b5ea52.pdf', 1, null, null);
INSERT INTO public.files (fileid, name, filepath, adduser, isdeleted, createdate, encodedname, documentid, extension, mimetype) VALUES (11, ' 2018.11.08_list_MEN_GIS_rodzice.pdf', '$DIR_PATH$2018-12-11', 1, false, '2018-12-11 18:38:15.390809', 'c11af2355e8b40c5.pdf', 1, null, null);
INSERT INTO public.files (fileid, name, filepath, adduser, isdeleted, createdate, encodedname, documentid, extension, mimetype) VALUES (12, ' 2018.11.08_list_MEN_GIS_rodzice.pdf', '$DIR_PATH$2018-12-11', 1, false, '2018-12-11 18:39:23.005872', '389453481afdcd44.pdf', 40, null, null);
INSERT INTO public.files (fileid, name, filepath, adduser, isdeleted, createdate, encodedname, documentid, extension, mimetype) VALUES (13, ' 2018.11.08_list_MEN_GIS_rodzice.pdf', '$DIR_PATH$2018-12-11', 1, false, '2018-12-11 18:43:30.342140', '44a7318f1162ae37.pdf', 41, null, null);
INSERT INTO public.files (fileid, name, filepath, adduser, isdeleted, createdate, encodedname, documentid, extension, mimetype) VALUES (14, ' 2018.11.08_list_MEN_GIS_rodzice.pdf', '$DIR_PATH$2018-12-11', 1, false, '2018-12-11 18:44:24.251645', '0d69b5524793f7c1.pdf', 42, null, null);
INSERT INTO public.files (fileid, name, filepath, adduser, isdeleted, createdate, encodedname, documentid, extension, mimetype) VALUES (15, ' 2018.11.08_list_MEN_GIS_rodzice.pdf', '$DIR_PATH$2018-12-11', 1, false, '2018-12-11 18:45:29.241116', 'dcfb7119aa8b0ed5.pdf', 43, null, null);
INSERT INTO public.files (fileid, name, filepath, adduser, isdeleted, createdate, encodedname, documentid, extension, mimetype) VALUES (16, ' 2018.11.08_list_MEN_GIS_rodzice.pdf', '$DIR_PATH$2018-12-11', 1, false, '2018-12-11 18:46:38.562228', '64bf724dc1ebed89.pdf', 44, null, null);
INSERT INTO public.files (fileid, name, filepath, adduser, isdeleted, createdate, encodedname, documentid, extension, mimetype) VALUES (17, ' 2018.11.08_list_MEN_GIS_rodzice.pdf', '$DIR_PATH$2018-12-11', 1, false, '2018-12-11 18:47:36.252010', '5e93364ca06a92db.pdf', 45, null, null);
INSERT INTO public.files (fileid, name, filepath, adduser, isdeleted, createdate, encodedname, documentid, extension, mimetype) VALUES (18, ' 2018.11.08_list_MEN_GIS_rodzice.pdf', '$DIR_PATH$2018-12-11', 1, false, '2018-12-11 18:48:09.858576', '3b7ade3b9326350a.pdf', 46, null, null);
INSERT INTO public.files (fileid, name, filepath, adduser, isdeleted, createdate, encodedname, documentid, extension, mimetype) VALUES (19, ' 2018.11.08_list_MEN_GIS_rodzice.pdf', '$DIR_PATH$2018-12-11', 1, false, '2018-12-11 18:49:06.407258', 'f66c119ed0a9a842.pdf', 47, null, null);
INSERT INTO public.files (fileid, name, filepath, adduser, isdeleted, createdate, encodedname, documentid, extension, mimetype) VALUES (20, ' 2018.11.08_list_MEN_GIS_rodzice.pdf', '$DIR_PATH$2018-12-11', 1, false, '2018-12-11 18:49:53.257415', '5c1b7e43e918d87a.pdf', 48, null, null);
INSERT INTO public.files (fileid, name, filepath, adduser, isdeleted, createdate, encodedname, documentid, extension, mimetype) VALUES (21, ' 2018.11.08_list_MEN_GIS_rodzice.pdf', '$DIR_PATH$2018-12-11', 1, false, '2018-12-11 19:02:17.346122', 'd30f1c72ccbbe25c.pdf', 49, null, null);
INSERT INTO public.files (fileid, name, filepath, adduser, isdeleted, createdate, encodedname, documentid, extension, mimetype) VALUES (22, ' 2018.11.08_list_MEN_GIS_rodzice.pdf', '$DIR_PATH$2018-12-11', 1, false, '2018-12-11 19:03:43.868691', '337575e4449d0665.pdf', 50, null, null);
INSERT INTO public.files (fileid, name, filepath, adduser, isdeleted, createdate, encodedname, documentid, extension, mimetype) VALUES (24, ' 2018.11.08_list_MEN_GIS_rodzice.pdf', '$DIR_PATH$2018-12-11', 1, false, '2018-12-11 19:10:00.648528', 'e968e2ce47f90e74.pdf', 52, null, null);
INSERT INTO public.files (fileid, name, filepath, adduser, isdeleted, createdate, encodedname, documentid, extension, mimetype) VALUES (25, ' 2018.11.08_list_MEN_GIS_rodzice.pdf', '$DIR_PATH$2018-12-11', 1, false, '2018-12-11 19:10:25.662717', '49714aef156c3c65.pdf', 53, null, null);
INSERT INTO public.files (fileid, name, filepath, adduser, isdeleted, createdate, encodedname, documentid, extension, mimetype) VALUES (26, ' 2018.11.08_list_MEN_GIS_rodzice.pdf', '$DIR_PATH$2018-12-11', 1, false, '2018-12-11 19:11:43.823098', 'd6c1a07c54e25fb8.pdf', 54, null, null);
INSERT INTO public.files (fileid, name, filepath, adduser, isdeleted, createdate, encodedname, documentid, extension, mimetype) VALUES (27, ' 2018.11.08_list_MEN_GIS_rodzice.pdf', '$DIR_PATH$2018-12-11', 1, false, '2018-12-11 19:15:25.960424', 'c75999d3f4619741.pdf', 55, null, null);
INSERT INTO public.files (fileid, name, filepath, adduser, isdeleted, createdate, encodedname, documentid, extension, mimetype) VALUES (28, ' 2018.11.08_list_MEN_GIS_rodzice.pdf', '$DIR_PATH$2018-12-11', 1, false, '2018-12-11 19:16:58.511812', '64257e68cba69506.pdf', 56, null, null);
INSERT INTO public.files (fileid, name, filepath, adduser, isdeleted, createdate, encodedname, documentid, extension, mimetype) VALUES (29, ' 2018.11.08_list_MEN_GIS_rodzice.pdf', '$DIR_PATH$2018-12-11', 1, false, '2018-12-11 19:17:03.495651', '35df0433bd925d81.pdf', 58, null, null);
INSERT INTO public.files (fileid, name, filepath, adduser, isdeleted, createdate, encodedname, documentid, extension, mimetype) VALUES (30, ' 2018.11.08_list_MEN_GIS_rodzice.pdf', '$DIR_PATH$2018-12-11', 1, false, '2018-12-11 19:19:23.874773', '853e11403d154bd5.pdf', 88, null, null);
INSERT INTO public.files (fileid, name, filepath, adduser, isdeleted, createdate, encodedname, documentid, extension, mimetype) VALUES (31, ' 2018.11.08_list_MEN_GIS_rodzice.pdf', '$DIR_PATH$2018-12-11', 1, false, '2018-12-11 19:38:39.869432', '479ac5002042111b.pdf', 95, 'pdf                                                                                                 ', '');
INSERT INTO public.files (fileid, name, filepath, adduser, isdeleted, createdate, encodedname, documentid, extension, mimetype) VALUES (32, ' 2018.11.08_list_MEN_GIS_rodzice.pdf', '$DIR_PATH$2018-12-11', 1, false, '2018-12-11 19:40:54.012780', '3a4ee183a3036b13.pdf', 98, 'pdf                                                                                                 ', '');
INSERT INTO public.files (fileid, name, filepath, adduser, isdeleted, createdate, encodedname, documentid, extension, mimetype) VALUES (33, ' 2018.11.08_list_MEN_GIS_rodzice.pdf', '$DIR_PATH$2018-12-11', 1, false, '2018-12-11 19:42:58.337765', 'd1241ff3d49c2eca.pdf', 100, 'pdf                                                                                                 ', '');
INSERT INTO public.files (fileid, name, filepath, adduser, isdeleted, createdate, encodedname, documentid, extension, mimetype) VALUES (34, ' 2018.11.08_list_MEN_GIS_rodzice.pdf', '$DIR_PATH$2018-12-11', 1, false, '2018-12-11 19:45:35.190867', '29a4e841f08186c8.pdf', 102, 'pdf                                                                                                 ', 'application/pdf');
create table roles
(
  roleid   serial       not null
    constraint roles_pkey
      primary key,
  rolename varchar(255) not null
);

alter table roles
  owner to documents;

create unique index roles_roleid_uindex
  on roles (roleid);

INSERT INTO public.roles (roleid, rolename) VALUES (1, 'Admin');
INSERT INTO public.roles (roleid, rolename) VALUES (2, 'User');
create table status
(
  statusid    serial  not null
    constraint status_pkey
      primary key,
  typeid      integer not null,
  documenttyp integer
    constraint status_documents_types_doctyp_fk
      references documents_types,
  name        varchar(255),
  color       varchar(7)
);

comment on column status.statusid is 'Identyfikator statusu
';

comment on column status.typeid is 'Typ statusu';

comment on column status.documenttyp is 'Typ dokumentu ''1 = DOCUMENTS'', ''2 = TASKS''';

comment on column status.name is 'Nazwa statusu';

alter table status
  owner to documents;

INSERT INTO public.status (statusid, typeid, documenttyp, name, color) VALUES (1, 1, 1, 'NOWY', '#FFFFFF');
INSERT INTO public.status (statusid, typeid, documenttyp, name, color) VALUES (2, 3, 1, 'ZAKOŃCZONY', '#FFFF00');
INSERT INTO public.status (statusid, typeid, documenttyp, name, color) VALUES (3, 2, 1, 'W TRAKCIE', '#FF00FF');
create table tasks
(
  taskid           serial                  not null
    constraint tasks_pkey
      primary key,
  name             varchar(255),
  description      text,
  start            timestamp default now(),
  "end"            timestamp default now(),
  createddate      timestamp default now(),
  lastupdate       timestamp default now(),
  adduser          integer
    constraint tasks_addusr_fkey
      references users,
  status           integer,
  modyfycationuser integer
    constraint tasks_modusr_fkey
      references users,
  isdeleted        boolean   default false not null
);

alter table tasks
  owner to documents;


create table users
(
  usrid       serial                  not null
    constraint users_pkey
      primary key
    constraint users_roles_roleid_fk
      references roles,
  firstname   varchar(255),
  createddate timestamp default now(),
  lastupdate  timestamp default now(),
  password    varchar(255)            not null,
  lastname    varchar(255),
  roleid      integer   default 2     not null,
  isdeleted   boolean   default false not null
);

comment on column users.usrid is 'Identyfikator użytkownika';

comment on column users.firstname is 'Nazwa użytkownika';

comment on column users.createddate is 'Data utworzenia konta';

comment on column users.lastupdate is 'Ostatnia aktualizacja';

alter table users
  owner to documents;

INSERT INTO public.users (usrid, firstname, createddate, lastupdate, password, lastname, roleid, isdeleted) VALUES (1, 'Paweł', '2018-11-21 17:45:58.867919', '2018-11-21 17:45:58.867919', 'wrerwerwerwfsfs', 'Konik', 1, false);