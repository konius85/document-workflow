<?php
/**
 * Created by PhpStorm.
 * User: konik
 * Date: 18.12.18
 * Time: 17:40
 */

class UsersController extends AbstractController
{
    public function __construct($app)
    {
        parent::__construct($app);
        $app->map(['GET'], '', 'getUsers');
        $app->map(['GET'], '{id}', 'getUserById');
        $app->map(['POST'], 'update', 'update');
        $app->map(['POST'], 'authenticate', 'authenticate');
    }

    public function getUsers($request)
    {
        $query = 'SELECT usrid as userId, usrid as uid, firstname, lastname, COALESCE(firstname || \' \' || lastname) as fullname, login, rolename FROM users u
        JOIN roles r on u.roleid = r.roleid WHERE u.isdeleted = false';
        $data['users'] = $this->db->query($query)->fetchAll();
        echo json_encode($data);
    }


    public function getUserById($request, $response, $args)
    {
        $userId = $args['id'];
        $query = 'SELECT usrid as userId, login, u.isdeleted, firstname, lastname,r.roleid, COALESCE(firstname || \' \' || lastname) as fullname, login, rolename FROM users u
        JOIN roles r on u.roleid = r.roleid WHERE u.isdeleted = false AND usrid = :userId';

        $result['roles'] = $this->db->query("SELECT roleid, rolename FROM roles")->fetchAll();

        $sth = $this->db->prepare($query);
        $sth->bindParam(':userId', $userId, PDO::PARAM_INT);
        try{
            $sth->execute();
            $result['user'] = $sth->fetchAll(PDO::FETCH_ASSOC);
        }catch(Exception $e){
           
            return $response->withJson($e, 200, JSON_PRETTY_PRINT);
        }
        return $response->withJson($result, 200, JSON_PRETTY_PRINT);
    }

    public function authenticate($request)
    {
        $params = $request->getParams();
        $login = $params['username'];
        $passwd = hash('sha512', $params['password']);
        $query = "SELECT usrid as userId, firstname as firstName, lastname as lastName, COALESCE(firstname || ' ' || lastname) as fullName FROM users u WHERE u.isdeleted = false AND u.password = :password AND u.login = :login";

        $sth = $this->db->prepare($query);
        $sth->bindParam(':login', $login, PDO::PARAM_STR);
        $sth->bindParam(':password', $passwd, PDO::PARAM_STR);
        try{
            $sth->execute();
            $result['user'] = $sth->fetchAll(PDO::FETCH_ASSOC);
            $result['token'] = md5(uniqid($login, true));
                if(count($result['user'])){
                    echo json_encode($result);
                }else{
                 throw new Exception('Zły login lub hasło');
                }
            } catch(Exception $e){
            echo json_encode($e->getMessage());
        }
        
    }


    public function update($request, $response) {
        $params = $request->getParams();
         $usrId = $params['usrId'];
         $firstName = $params['firstName'];
         $lastName = $params['lastName'];
         $login = $params['login'];
         $role = $params['role'] ? $params['role'] : 2;
         $password = $params['password'];
         $isDeleted = $params['isDeleted'] == true ? true : false;
         $query = null;

         if(!empty($password) && strlen($password) > 7){
            $passwd = hash('sha512', $params['password']);
            $query = "UPDATE users SET firstname = :firstName, lastname = :lastName, isdeleted = :isDeleted, login = :login, roleid = :roleId,
                                             lastupdate = now(), password = :password WHERE usrid = :usrId";
            $sth = $this->db->prepare($query);
            $sth->bindParam(':password', $passwd, PDO::PARAM_STR);

         }else{
             $query = "UPDATE users SET firstname = :firstName, lastname = :lastName, isdeleted = :isDeleted, login = :login, roleid = :roleId,
                                             lastupdate = now() WHERE usrid = :usrId";
             $sth = $this->db->prepare($query);
         }

         $sth->bindParam(':firstName', $firstName, PDO::PARAM_STR);
         $sth->bindParam(':lastName', $lastName, PDO::PARAM_STR);
         $sth->bindParam(':isDeleted', $isDeleted, PDO::PARAM_BOOL);
         $sth->bindParam(':login', $login, PDO::PARAM_STR);
         $sth->bindParam(':roleId', $role, PDO::PARAM_INT);
         $sth->bindParam(':usrId', $usrId, PDO::PARAM_INT);

        try{
            $sth->execute();
        }catch(Exception $e){
            return $response->withJson($e, 404, JSON_PRETTY_PRINT);
        }

        return $response->withJson($params, 200, JSON_PRETTY_PRINT);

    }


}
