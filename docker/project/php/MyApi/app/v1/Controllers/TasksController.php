<?php

class TasksController extends ParamsController
{

    private $db;
    private $app;

    public function __construct($app)
    {
        //parent::__construct($app);
         $this->app = $app;
                $container = $app->getContainer();
                $this->db = $container['db'];

         $app->map(['GET'], '', 'getTasks');
         $app->map(['GET'], '/{id}', 'getTaskById');
         $app->map(['GET'], '/today', 'getTodayTask');
         $app->map(['GET'], '/day', 'getTaskForDay');
         $app->map(['POST'], '/update', 'update');
         $app->map(['POST'], '/add', 'addTask');
         $app->map(['GET'], '/remove/{taskId}', 'remove');
         $app->map(['GET'], '/document/{docId}', 'getTaskForDocument');
    }

    public function getTasks($request, $response)
    {
        $params = $request->getParams();
        $fields = array('field' => 't.name');
        $resultQuery = $this->prepareTasksParams($params, $fields, $request);
        $query = "SELECT t.*, COALESCE(u.firstname, '') || ' ' || COALESCE(u.lastname, '') as fullname,
                CASE WHEN t.iscompleted THEN 'fas fa-check' ELSE 'fas fa-times' END as icon FROM tasks t LEFT JOIN users u on t.userid = u.usrid
                 $resultQuery ORDER BY t.start desc";
        try{
                $sth = $this->db->prepare($query);
                $sth->execute();
                $result['tasks'] = $sth->fetchAll(PDO::FETCH_ASSOC);
        }catch(Exception $e){
          var_dump($sth);
        }
         return $response->withJson($result, 200, JSON_PRETTY_PRINT);
    }


    public function getTaskById($request, $response, $args)
    {
        $taskId = $args['id'];
        $query = "SELECT d.documentid || ' ' || d.name as docName, t.* FROM tasks t
                  LEFT JOIN  documents d ON(t.docid = d.documentid)
                  WHERE t.taskid = :taskId AND t.isdeleted IS FALSE";
        $sth = $this->db->prepare($query);
        $sth->bindParam(':taskId', $taskId, PDO::PARAM_INT);
        $sth->execute();
        $result['task'] = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $response->withJson($result, 200, JSON_PRETTY_PRINT);
    }

    public function updateDocument($request)
    {
       $params = $request->getParams();
       $taskId = $params['taskId'];
       $name = $params['name'];
       $description = $params['description'];
       $modUser = $params['modUser'];
       $userId = $params['userId'];
       $docId = $params['taskDocument'];

        $sth = $this->db->prepare('UPDATE tasks SET name = :name, description = :description, modyfycationuser = :modUser, docid=:docId WHERE taskid = :taskId');
        $sth->bindParam(':taskid', $taskId, PDO::PARAM_INT);
        $sth->bindParam(':name', $name, PDO::PARAM_STR);
        $sth->bindParam(':description', $description, PDO::PARAM_STR);
        $sth->bindParam(':modUser', $modUser, PDO::PARAM_INT);
        $sth->bindParam(':docId', $docId, PDO::PARAM_INT);
        $sth->execute();
    }


    public function getTodayTask($request, $response)
    {
        $userId = $this->getUserId($request);
        $query = 'SELECT * FROM tasks WHERE ((start::date <= now()::date AND enddate::date >= now()) OR
                                            (start::date = now()::date))
                                        AND iscompleted IS FALSE AND isdeleted is FALSE AND userid = :userId';
        $sth = $this->db->prepare($query);
        $sth->bindParam(':userId', $userId, PDO::PARAM_INT);
        try{
          $sth->execute();
          $result['tasks'] = $sth->fetchAll(PDO::FETCH_ASSOC);
        }catch(Exception $e){
          return $response->withJson($e, 200, JSON_PRETTY_PRINT);
        }
        return $response->withJson($result, 200, JSON_PRETTY_PRINT);
    }

    public function getTaskForDay($request, $response)
        {
            $userId = $this->getUserId($request);
            $params = $request->getParams();
            $date = $params['date'];
            $query = 'SELECT * FROM tasks WHERE start::date <= :activeDay::date AND enddate >= :activeDay::date AND iscompleted IS FALSE AND isdeleted is FALSE AND userid = :userId';
            $sth = $this->db->prepare($query);
            $sth->bindParam(':userId', $userId, PDO::PARAM_INT);
            $sth->bindParam(':activeDay', $date, PDO::PARAM_STR);
            try{
              $sth->execute();
              $result['tasks'] = $sth->fetchAll(PDO::FETCH_ASSOC);
            }catch(Exception $e){
              return $response->withJson($e, 200, JSON_PRETTY_PRINT);
            }
            return $response->withJson($result, 200, JSON_PRETTY_PRINT);
        }

    public function getTaskForDocument($request, $response, $args)
    {
        $docId = $args['docId'];
        $query = 'SELECT t.*, COALESCE(u.firstname, \'\') || \' \' ||COALESCE(u.lastname, \'\') as employes,
                    CASE WHEN t.iscompleted THEN \'fas fa-check\' ELSE \'fas fa-times\' END as icon
                    FROM tasks t
                    LEFT JOIN users u ON u.usrid = t.userid
                  WHERE t.docid = :docId AND t.isdeleted IS false';

        $sth = $this->db->prepare($query);
        $sth->bindParam(':docId', $docId, PDO::PARAM_INT);
        $sth->execute();
        $result['tasks'] = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $response->withJson($result, 200, JSON_PRETTY_PRINT);
    }

    public function update($request, $response){
        $params = $request->getParams();
        $taskName = $params['taskName'];
        $taskDescription = $params['taskDescription'];
        $fromDate = $params['fromDate'] ? new DateTime($params['fromDate']) : null;
        $formatedFrom = $fromDate ? $fromDate->format('Y-m-d H:i:s') : null;

        $toDate = $params['toDate'] ? new DateTime($params['toDate']) : null;
        $formatedTo = $toDate ? $toDate->format('Y-m-d H:i:s') : '';

        $user = $params['user'];
        $modUser = $params['modUser'];
        $isCompleted = $params['isCompleted'];
        $taskId = $params['taskId'];
        $docId = $params['taskDocument'];
        $todayDay = new DateTime('now');
        $today = $todayDay->format('Y-m-d H:i:s');

        $sql = "UPDATE tasks SET name=:taskName, description=:taskDescription, start=:fromDate, enddate=:toDate,isCompleted=:isCompleted,  userid=:user, lastupdate=:lastUpdate, docid=:docId  WHERE taskid=:taskId";
        $sth = $this->db->prepare($sql);
        $sth->bindParam(':taskName', $taskName, PDO::PARAM_STR);
        $sth->bindParam(':taskDescription', $taskDescription, PDO::PARAM_STR);
        $sth->bindParam(':fromDate', $formatedFrom, PDO::PARAM_STR);
        $sth->bindParam(':toDate', $formatedTo, PDO::PARAM_STR);
        $sth->bindParam(':user', $user, PDO::PARAM_INT);
        $sth->bindParam(':taskId', $taskId, PDO::PARAM_INT);
        $sth->bindParam(':isCompleted', $isCompleted, PDO::PARAM_BOOL);
        $sth->bindParam(':lastUpdate', $today, PDO::PARAM_STR);
        $sth->bindParam(':docId', $docId, PDO::PARAM_INT);
        try{
            $sth->execute();
        }catch(PDOException $eP){
            return $response->withJson('Bład zapytania', 404, JSON_PRETTY_PRINT);
        }catch(Exception $e){
            return $response->withJson('Bład', 404, JSON_PRETTY_PRINT);
        }
        
        return $response->withJson($params, 200, JSON_PRETTY_PRINT);
    }

    public function addTask($request, $response)
    {
        $params = $request->getParams();
        $taskName = $params['taskName'];
        $taskDescription = $params['taskDescription'];
        $fromDate = $params['fromDate'] ? new DateTime($params['fromDate']) : null;
        $formatedFrom = $fromDate ? $fromDate->format('Y-m-d H:i:s') : null;

        $toDate = $params['toDate'] ? new DateTime($params['toDate']) : null;
        $formatedTo = $toDate ? $toDate->format('Y-m-d H:i:s') : '';

        $user = $params['modUser'];
        $isCompleted = $params['isCompleted'] == true? true : false;
        $docId = $params['taskDocument'];
        $todayDay = new DateTime('now');
        $today = $todayDay->format('Y-m-d H:i:s');
     
        $sql = "INSERT INTO tasks (name, description, start, enddate, adduser, userid, iscompleted, docid)
        VALUES (:taskName, :taskDescription, :fromDate, :toDate, :user, :modUser, :isCompleted, :docId) RETURNING taskid";
        $sth = $this->db->prepare($sql);

        $sth->bindParam(':taskName', $taskName, PDO::PARAM_STR);
        $sth->bindParam(':taskDescription', $taskDescription, PDO::PARAM_STR);
        $sth->bindParam(':fromDate', $formatedFrom, PDO::PARAM_STR);
        $sth->bindParam(':toDate', $formatedTo, PDO::PARAM_STR);
        $sth->bindParam(':user', $user, PDO::PARAM_INT);
        $sth->bindParam(':modUser', $user, PDO::PARAM_INT);
        $sth->bindParam(':isCompleted', $isCompleted, PDO::PARAM_BOOL);
        $sth->bindParam(':docId', $docId, PDO::PARAM_INT);
        try{
            $sth->execute();
            $result = $sth->fetchAll();
        }catch(PDOException $e){
            return $response->withJson($e, 200, JSON_PRETTY_PRINT);
        }
        return $response->withJson($result, 200, JSON_PRETTY_PRINT);
    }


    public function remove($request, $response, $args){
      $taskId = $args['taskId'];

       $sth = $this->db->prepare('UPDATE tasks SET isdeleted=true WHERE taskid = :taskId');
        $sth->bindParam(':taskId', $taskId, PDO::PARAM_INT);
       $sth->execute();
       return $response->withJson($taskId, 200, JSON_PRETTY_PRINT);
    }
}
