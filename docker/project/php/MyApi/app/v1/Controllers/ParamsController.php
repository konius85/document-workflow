<?php
/**
 * Created by PhpStorm.
 * User: konik
 * Date: 24.11.18
 * Time: 12:29
 */

class ParamsController
{


    public function __construct()
    {
    }

    public function getUserId($request){
      $headers = $request->getHeaders('Authorization');
          return str_replace('Bearer ', '',$headers['HTTP_AUTHORIZATION'][0]);
    }

    protected function prepareDocumentsParams($params, $fields, $request)
    { 
        $userId = $this->getUserId($request);
        $query = ' WHERE doc.isdeleted is FALSE AND doc.userid = ' . $userId .
        'OR doc.documentid IN(SELECT docid FROM tasks WHERE userid ='. $userId . ')';
        if (isset($params['q'])) {
            $query .= ' AND lower('. $fields['field'].')  LIKE \'%' . $params['q'] . '%\'';
        }
        return $query;
    }

    protected function prepareTasksParams($params, $fields, $request)
    {
        $userId = $this->getUserId($request);
        $query = 'WHERE t.isdeleted is FALSE AND t.userid = '. $userId;
        if (isset($params['q'])) {
            $query .= ' AND lower('. $fields['field'].')  LIKE \'%' . $params['q'] . '%\'';
        }
        return $query;
    }
}

