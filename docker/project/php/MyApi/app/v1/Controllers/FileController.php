<?php

use Slim\Http\UploadedFile;

/**
 * Created by PhpStorm.
 * User: konik
 * Date: 09.12.18
 * Time: 15:43
 */
class FileController extends UploadedFile
{

    private $db;
    private $directory;

    public function __construct($app)
    {
        $container = $app->getContainer();
        $this->db = $container['db'];
        $this->directory = $container['uploadPath'];
        // $app->map(['POST'], '', 'uploadFile');
    }

    public function uploadFile($request)
    {
        // $this->db = $container['db'];
        $params = $request->getParams();

        // $this->db->beginTransaction();
        try {
            $this->createPath();
            $uploadedFiles = $request->getUploadedFiles();
            $fileName = $uploadedFiles['file']->getClientFilename();
            if ($uploadedFiles['file']->getError() === UPLOAD_ERR_OK) {
                $fileData = $this->moveUploadedFile($uploadedFiles['file']);
            }
            $shortPath = $this->directory['shortPath'];

            $fileHex = $fileData['fileName'];
            $mimeType = $fileData['mimeType'];
            $extension = $fileData['extension'];
           
            $userId = $params['user'];
            $result = null;
            $fileId = null;
            
             $result = $this->db->query("INSERT INTO files (name, encodedname, mimetype, extension, filePath, adduser )
                              VALUES ('$fileName', '$fileHex', '$mimeType', '$extension', '$shortPath', '$userId') RETURNING fileid")->fetch();
            $fileId = $result['fileid'];
            
            // $this->db->commit();
            return $fileId;
        } catch (Exception $e) {
            echo json_encode($e);
            // $this->db->rollBack();
        }

    }

    private function createPath()
    {
        if (!file_exists($this->directory['fullPath'])) {
            if (!mkdir($this->directory['fullPath'], 0775, true)) {
                die('Cannot create file path');
            };
        }
    }


    public function moveUploadedFile(UploadedFile $uploadedFile)
    {
        $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
        $basename = bin2hex(random_bytes(8));
        $filename = sprintf('%s.%0.8s', $basename, $extension);
        try {
            $uploadedFile->moveTo($this->directory['fullPath'] . DIRECTORY_SEPARATOR . $filename);
        } catch (Exception $e) {
            throw new Exception('Cannot move file');
        }
        $mimeType = mime_content_type($this->directory['fullPath'] . DIRECTORY_SEPARATOR . $filename);
        return array('fileName' => $filename, 'extension' => $extension, 'mimeType' => $mimeType);
    }
}
