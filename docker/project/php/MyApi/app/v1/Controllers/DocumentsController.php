<?php

/**
 * Created by PhpStorm.
 * User: Paweł Konik
 * Date: 21.11.18
 * Time: 17:48
 */

class DocumentsController extends ParamsController
{
    private $db;
    private $app;

    public function __construct($app)
    {
        $this->app = $app;
        $container = $app->getContainer();
        $this->db = $container['db'];
        $app->map(['GET'], '', 'getDocuments');
        $app->map(['GET'], '/documentsTypes', 'getDocumentsTypes');
        $app->map(['POST'], '/add', 'addDocument');
        $app->map(['POST'], '/update', 'updateDocument');
        $app->map(['POST'], '/removeDocument', 'removeDocument');
        $app->map(['GET'], '/{id}', 'getDocumentById');
        $app->map(['GET'], '/name/{name}', 'DocumentsController:getDocumentByName');

    }

    public function getDocuments($request, $response)
    {
        $params = $request->getParams();

        $fields = array('field' => 'doc.name');
        $result = $this->prepareDocumentsParams($params, $fields, $request);

        $query = "
            SELECT doc.documentid, doc.description, doc.name as name, doc.createddate as adddat, doc.lastupdate as lastup,
       COALESCE(u.firstname || ' ' || u.lastname, '') as user__,
       json_build_object('name', s2.name, 'color', s2.color) as status, t.name as type__,
                   json_build_object('originalname', f.name,'path', f.filepath, 'name', f.encodedname,'extension', f.extension) as file
        FROM documents doc
       LEFT JOIN users u on doc.adduserid = u.usrid
       LEFT JOIN status s2 on doc.statusid = s2.statusid
       LEFT JOIN documents_types t on doc.documenttypeid = t.documenttypeid
       LEFT JOIN files f ON( f.fileid = doc.fileid)" . $result . ' ORDER BY doc.createddate desc';

        $data['data'] = $this->db->query($query)->fetchAll();
        foreach ($data['data'] as &$da) {
            if ($da['status']) {
                $da['status'] = json_decode($da['status']);
            }
            if ($da['file']) {
                $da['file'] = json_decode($da['file']);
            }
        }
         return $response->withJson($data, 200, JSON_PRETTY_PRINT);
    }

    public function addDocument($request)
    {
        
        $params = $request->getParams();
        $documentName = $params['documentName'];
        $type = $params['type'];
        $description = $params['description'];
        $userId = $params['user'];
        
        $this->db->beginTransaction();
        try{
            $docId = $this->db->query("INSERT INTO documents (name, adduserid, statusid, documenttypeid, description, userid)
            VALUES ('$documentName', $userId, 1, '$type', '$description', $userId) RETURNING documentid")->fetch();
            $doc = $docId['documentid'];
            $data['docId'] = $doc;
            if($params['file']){
                $fileController = new FileController($this->app);
                $fileId = $fileController->uploadFile($request);
               
                $sth = $this->db->prepare('UPDATE documents SET fileid = :fileId WHERE documentid = :doc');
                $sth->bindParam(':doc', $doc, PDO::PARAM_STR);
                $sth->bindParam(':fileId', $fileId, PDO::PARAM_STR);
                $sth->execute();

                $sth = $this->db->prepare('UPDATE files SET documentid = :doc WHERE fileid = :fileId');
                $sth->bindParam(':doc', $doc, PDO::PARAM_STR);
                $sth->bindParam(':fileId', $fileId, PDO::PARAM_STR);
                $sth->execute();
                $data['fileId'] = $fileId; 
            }
            $this->db->commit();
            
            echo json_encode($data);
        } catch (Exception $e) {
            $this->db->rollBack();
            var_dump($e);
        }
   }

   public function updateDocument($request) {
       $params = $request->getParams();
       $docId = $params['docId'];
       $fileName = $params['file'];
       $description = $params['description'];
       $name = $params['documentName'];
       $userId = $params['user'];
       $modUser = $params['modificationUserId'];
       $status = $params['status'];
       $removeFile = $params['removeFile'];

       $query = 'SELECT * FROM documents WHERE documentid = :docId';
       $sth = $this->db->prepare($query);
       $sth->bindParam(':docId', $docId, PDO::PARAM_INT);
       try{
            $sth->execute();  
       } catch(Exception $e) {
            echo json_encode($e->getMessage());
       }
       $result = $sth->fetchAll(PDO::FETCH_ASSOC)[0];
       if(!$result['fileid'] && $fileName){
        //    UPDATE FILE
        $fileController = new FileController($this->app);
        $fileId = $fileController->uploadFile($request);
       
        $sth = $this->db->prepare('UPDATE documents SET fileid = :fileId WHERE documentid = :docId');
        $sth->bindParam(':docId', $docId, PDO::PARAM_STR);
        $sth->bindParam(':fileId', $fileId, PDO::PARAM_STR);
        $sth->execute();

        $sth = $this->db->prepare('UPDATE files SET documentid = :docId WHERE fileid = :fileId');
        $sth->bindParam(':docId', $docId, PDO::PARAM_STR);
        $sth->bindParam(':fileId', $fileId, PDO::PARAM_STR);
        $sth->execute();
       }

        if($removeFile == "true"){
           $queryFile = 'UPDATE files SET isdeleted=true WHERE documentid = :docId';
           $sth = $this->db->prepare($queryFile);
           $sth->bindParam(':docId', $docId, PDO::PARAM_STR);
           $sth->execute();
           $queryFile1 = 'UPDATE documents SET fileid=null WHERE documentid = :docId';
           $sth = $this->db->prepare($queryFile1);
           $sth->bindParam(':docId', $docId, PDO::PARAM_STR);
           $sth->execute();

        }
       //    UPDATE DOCUMENT
       $query = 'UPDATE documents SET userid=:userId, name = :name, statusid = :status, modificationuserid = :modUser, description = :desc, lastupdate = now() WHERE documentid = :docId';
       $sth = $this->db->prepare($query);
        $sth->bindParam(':name', $name, PDO::PARAM_STR);
        $sth->bindParam(':status', $status, PDO::PARAM_INT);
        $sth->bindParam(':modUser', $modUser, PDO::PARAM_INT);
        $sth->bindParam(':desc', $description, PDO::PARAM_STR);
        $sth->bindParam(':docId', $docId, PDO::PARAM_INT);
        $sth->bindParam(':userId', $userId, PDO::PARAM_INT);
        try{
            $sth->execute();  
            echo json_encode($params);
       } catch(Exception $e) {
            echo json_encode($e->getMessage());
       }
   }

    public function getDocumentsTypes()
    {
        $query = 'SELECT documenttypeid as type, name FROM documents_types ORDER BY documenttypeid';
        $data['data'] = $this->db->query($query)->fetchAll();
        echo json_encode($data);
    }

    public function removeDocument($request)
    {
        $params = $request->getParams();
        $query = 'UPDATE documents SET isdeleted = true WHERE documentid = ' . $params['docId'] . ' RETURNING documentid' ;
        $data = $this->db->query($query)->fetchAll();
        echo json_encode($data);
    }

    public function getDocumentById($request, $response, $args)
    {
        $docId = $args['id'];
        $sth = $this->db->prepare('SELECT d.*, f.name as filename, f.createdate as filecreated  FROM documents d
                                   LEFT JOIN files f ON (d.fileid = f.fileid)
                                   WHERE d.documentid = :docId AND d.isdeleted = FALSE');
        $sth->bindParam(':docId', $docId, PDO::PARAM_INT);
        $sth->execute();
        $result =  $sth->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode($result);
    }


    public function getDocumentByName($request, $response, $args)
    {
        $name = $args['name'];
        $fullString = '%' . $name . '%';
        $sth = $this->db->prepare('SELECT documentid || \' \' || name as docname, * FROM documents WHERE isdeleted = FALSE AND name LIKE :name');
        $sth->bindParam(':name', $fullString, PDO::PARAM_STR);
        try{
            $sth->execute();
            $result['documents'] =  $sth->fetchAll(PDO::FETCH_ASSOC);
        }catch(Exception $e){
            return $response->withJson($e, 404, JSON_PRETTY_PRINT);
        }
        
        return $response->withJson($result, 200, JSON_PRETTY_PRINT);
    }

}
