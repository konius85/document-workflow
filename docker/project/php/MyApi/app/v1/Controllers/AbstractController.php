<?php
/**
 * Created by PhpStorm.
 * User: konik
 * Date: 18.12.18
 * Time: 17:43
 */

abstract class AbstractController
{
    protected $db;

    public function __construct($app)
    {
        $container = $app->getContainer();
        $this->db = $container['db'];
    }
}