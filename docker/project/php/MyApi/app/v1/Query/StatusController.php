<?php
/**
 * Created by PhpStorm.
 * User: konik
 * Date: 24.11.18
 * Time: 17:54
 */

class StatusController
{
    private $db;

    public function __invoke()
    {
        // TODO: Implement __invoke() method.
    }

    public function __construct($app)
    {
        $container = $app->getContainer();
        $this->db = $container['db'];

        $app->map(['GET'], '', 'getStatus');
    }

    public function getStatus($request)
    {
        $params = $request->getParams();
        $documentType = $params['documentType'] ;
        $query = 'SELECT s.statusid, s.name FROM status s WHERE documenttyp = ' . $documentType . ' ORDER BY s.statusid';

        $result = $this->db->query($query)->fetchAll();
        echo json_encode($result);
    }
}
