import { TestBed } from '@angular/core/testing';

import { ResourceType, UrlService } from './url.service';

describe('UrlService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UrlService = TestBed.get(UrlService);
    expect(service).toBeTruthy();
  });

  it('should return properly url', function () {
    const service: UrlService = TestBed.get(UrlService);
    const url = service.getUrl(ResourceType.WEB, 'testUrl');
    expect(url).toBe('http://localhost/v1/testUrl');
  });
});
