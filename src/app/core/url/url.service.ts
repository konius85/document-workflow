import { Injectable } from '@angular/core';

export enum ResourceType {
  WEB = 'http://localhost/v1/',
  MONGO = 'http://localhost:2017/',
  ELASTICSEARCH = 'http://localhost:9200/',
}

@Injectable({
  providedIn: 'root'
})
export class UrlService {

  constructor() { }

  public getUrl(resourceType: ResourceType, url) {
    return resourceType + url;
  }
}
