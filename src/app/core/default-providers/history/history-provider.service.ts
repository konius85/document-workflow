import { Injectable } from '@angular/core';
import { ResourceType, UrlService } from '../../url/url.service';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from '../../auth/authentication.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HistoryProviderService {

  constructor(
    private urlService: UrlService,
    private http: HttpClient,
    private authService: AuthenticationService,
  ) {
  }

  public getHistory() {
    const url = this.urlService
      .getUrl(ResourceType.ELASTICSEARCH, 'history/_doc/_search?q=userId:' + this.authService.userId.toString());
    return this.http.get(url).pipe(map(hits => hits['hits']));
  }
}
