export enum DocumentAction {
  AddDocument = 1,
  EditDocument = 2,
  RemoveDocument = 3,
  AddTask = 4,
  EditTask = 5,
  RemoveTask = 6,
  ChangeField = 7,
}
