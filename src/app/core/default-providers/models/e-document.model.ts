import { DocumentAction } from './document-action.model';

export interface EDocumentModel {
  id?: string;
  action?: DocumentAction;
  name: string;
  documentId?: number;
  taskId?: number;
  userId: number;
  fullName?: string;
  creationDate?: string;
}


