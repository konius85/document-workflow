export interface UsersModel {
  userId: number;
  firstName: string;
  lastName: string;
  fullName: string;
}
