import { EDocumentModel } from './e-document.model';

export interface HistoryModel extends EDocumentModel {
  fieldName: string;
  from: string;
  to: string;
}
