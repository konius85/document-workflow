import { Injectable } from '@angular/core';
import { ResourceType, UrlService } from '../../url/url.service';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TaskProviderService {

  constructor(
    private urlService: UrlService,
    private http: HttpClient,
  ) {
  }

  public getTasks() {
    const url = this.urlService.getUrl(ResourceType.WEB, `tasks`);
    return this.http.get(url);
  }

  public getTasksByName(name: string): Observable<any> {
    const url = this.urlService.getUrl(ResourceType.WEB, `tasks?q=${ name }`);
    return this.http.get(url);
  }

  public getTasksForDate(date: string): Observable<any> {
    const url = this.urlService.getUrl(ResourceType.WEB, `tasks/day?date=${ date }`);
    return this.http.get(url);
  }

  public getTaskById(taskId: number) {
    const url = this.urlService.getUrl(ResourceType.WEB, `tasks/${ taskId }`);
    return this.http.get(url);
  }

  public addTask(taskData) {
    const url = this.urlService.getUrl(ResourceType.WEB, `tasks/add`);
    return this.http.post(url, taskData);
  }

  public updateTask(taskData) {
    const url = this.urlService.getUrl(ResourceType.WEB, `tasks/update`);
    return this.http.post(url, taskData);
  }

  public getTodayTask() {
    const url = this.urlService.getUrl(ResourceType.WEB, `tasks/today`);
    return this.http.get(url).pipe(map(tasks => tasks['tasks']));
  }

  public getTaskForDocument(docId: string) {
     const url = this.urlService.getUrl(ResourceType.WEB, `tasks/document/${docId}`);
    return this.http.get(url).pipe(map(tasks => tasks['tasks']));
  }

  public removeTask(taskId: string) {
    const url = this.urlService.getUrl(ResourceType.WEB, `tasks/remove/${taskId}`);
    return this.http.get(url).pipe(map(tasks => tasks['tasks']));
  }

}
