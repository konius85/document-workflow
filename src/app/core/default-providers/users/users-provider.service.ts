import { Injectable } from '@angular/core';
import { ResourceType, UrlService } from '../../url/url.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsersProviderService {

  constructor(
    private urlService: UrlService,
    private http: HttpClient,
  ) {
  }

  public getUsers() {
    const url = this.urlService.getUrl(ResourceType.WEB, `users`);
    return this.http.get(url);
  }

  public getUserById(userId: string){
    const url = this.urlService.getUrl(ResourceType.WEB, `users/${userId}`);
    return this.http.get(url);
  }

  public updateUser(data){
    const url = this.urlService.getUrl(ResourceType.WEB, `users/update`);
    return this.http.post(url, data);

  }

}
