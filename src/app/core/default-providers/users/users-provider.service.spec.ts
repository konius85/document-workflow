import { TestBed } from '@angular/core/testing';

import { UsersProviderService } from './users-provider.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('UsersProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
  }));

  it('should be created', () => {
    const service: UsersProviderService = TestBed.get(UsersProviderService);
    expect(service).toBeTruthy();
  });
});
