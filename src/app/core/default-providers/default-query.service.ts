import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ResourceType, UrlService } from '../url/url.service';
import { DocumentTypeEnum } from '../models/documentType.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, share } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DefaultQueryService {
  private _users$: BehaviorSubject<any> = new BehaviorSubject([]);

  public get users$() {
    if (!this._users$.value.length) {
      return this.getUsers();
    } else {
      return this._users$;
    }
  }

  constructor(
    private http: HttpClient,
    private urlService: UrlService,
  ) {
  }

  getStatus(documentType: DocumentTypeEnum): Observable<any> {
    const url = this.urlService.getUrl(ResourceType.WEB, `status?documentType=${ documentType }`);
    return this.http.get(url);
  }

  getDocumentsType(): Observable<any> {
    const url = this.urlService.getUrl(ResourceType.WEB, 'documents/documentsTypes');
    return this.http.get(url).pipe(map(data => data['data']));
  }

  public getUsers() {
    const url = this.urlService.getUrl(ResourceType.WEB, 'users');
    return this.http.get(url)
      .pipe(
        map(data => this.prepareUsers(data['users'])),
        share()
      );
  }

  uploadFile(data) {
    const url = this.urlService.getUrl(ResourceType.WEB, 'documents/add');
    return this.http.post(url, data);
  }

  prepareUsers(users = []) {
    const hits = [];
    users.forEach(user => {
      const { userid, firstname, lastname, fullname } = user;
      const hitModel = {
        userId: userid,
        firstName: firstname,
        lastName: lastname,
        fullName: fullname,
      };
      hits.push(hitModel);
    });
    return hits;
  }
}
