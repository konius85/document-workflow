import { Injectable } from '@angular/core';
import { ResourceType, UrlService } from '../../url/url.service';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { DocumentModel } from '../../../documents/models/document.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DocumentProviderService {

  constructor(
    private urlService: UrlService,
    private http: HttpClient,
  ) {
  }

  public getDocuments(): Observable<any> {
    const url = this.urlService.getUrl(ResourceType.WEB, `documents`);
    return this.http.get(url)
      .pipe(map(data => data['data']));
  }


  public getDocumentById(documentId: number) {
    const url = this.urlService.getUrl(ResourceType.WEB, `documents/${ documentId }`);
    return this.http.get(url).pipe(map(data => {
      return this.prepareDocumentData(data[0]);
    }));
  }

  public getDocumentByName(name: string) {
    const url = this.urlService.getUrl(ResourceType.WEB, `documents/name/${name}`);
    return this.http.get(url)
    .pipe(
      map(data => data['documents'].map(doc => this.prepareDocumentData(doc)))
    );
  }

  public updateDocument(formData) {
    const url = this.urlService.getUrl(ResourceType.WEB, `documents/update`);
    return this.http.post(url, formData);
  }

  private prepareDocumentData(data: Object): DocumentModel {
    return {
      documentId: data['documentid'],
      docName: data['docname'],
      name: data['name'],
      description: data['description'],
      createDate: data['createddate'],
      documentTypeId: data['documenttypeid'],
      fileId: data['fileid'],
      lastUpdate: data['lastupdate'],
      modificationUserId: data['modificationuserid'],
      statusId: data['statusid'],
      addUserId: data['adduserid'],
      userId: data['userid'],
      fileName: data['filename'] ? data['filename'] : null,
      fileCreated: data['filecreated'] ?  data['filecreated'] : null,
    };
  }


}
