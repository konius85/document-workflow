import { TestBed } from '@angular/core/testing';

import { DocumentProviderService } from './document-provider.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('DocumentProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
  }));

  it('should be created', () => {
    const service: DocumentProviderService = TestBed.get(DocumentProviderService);
    expect(service).toBeTruthy();
  });
});
