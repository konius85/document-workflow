import { Injectable } from '@angular/core';
import { ResourceType, UrlService } from '../../url/url.service';
import { delay, map, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { EDocumentModel } from '../models/e-document.model';
import { ReplaySubject, Subject } from 'rxjs';
import { AuthenticationService } from '../../auth/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  observeWhenAddNewNotification$: Subject<boolean> = new Subject();

  constructor(
    private urlService: UrlService,
    private http: HttpClient,
    private authService: AuthenticationService,
  ) {
  }


  public getNotifications() {
    const requestData =
      {
        'query': {
          'match': {
            'userId': this.authService.userId
          }
        },
        'sort': {
          'creationDate': {
            'order': 'desc'
          }
        },
        'size': 1000
      };
    const url = this.urlService
      .getUrl(ResourceType.ELASTICSEARCH, 'notifications/_doc/_search');
    return this.http.post(url, requestData).pipe(map(data => data['hits']));
  }

  public addNotification(notification: EDocumentModel) {
    const url = this.urlService.getUrl(ResourceType.ELASTICSEARCH, 'notifications/_doc');
    return this.http.post(url, notification).pipe(delay(2000));
  }

  public deleteNotification(documentId: string) {
    const url = this.urlService.getUrl(ResourceType.ELASTICSEARCH, 'notifications/_doc/' + documentId);
    return this.http.delete(url);
  }

}
