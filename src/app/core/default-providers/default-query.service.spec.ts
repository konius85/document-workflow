import { inject, TestBed } from '@angular/core/testing';

import { DefaultQueryService } from './default-query.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { UrlService } from '../url/url.service';
class MockUrlService {
  getUrl() {
    return 'http://localhost/';
  }
}


describe('DefaultQueryService', () => {
  // let httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
  let service: DefaultQueryService;

  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [
      { provide: UrlService, useClass: MockUrlService },
    ]
  }));

  beforeEach(() => {
    service = TestBed.get(DefaultQueryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });


});
