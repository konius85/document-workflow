import { TestBed } from '@angular/core/testing';

import { TokenInterceptorService } from './token-interceptor.service';
import { AuthenticationService } from './authentication.service';

class MockAuthenticationService {
  
}

describe('TokenInterceptorService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      { provide: AuthenticationService, useClass: MockAuthenticationService }
    ]
  }));

  it('should be created', () => {
    const service: TokenInterceptorService = TestBed.get(TokenInterceptorService);
    expect(service).toBeTruthy();
  });
});
