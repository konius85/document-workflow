import { TestBed } from '@angular/core/testing';

import { AdminGuardService } from './admin-guard.service';
import { Router } from '@angular/router';
import { AuthenticationService } from './authentication.service';

class MockRouter {
  navigate() {
  }
}

class MockAuthenticationService {
  currentUserValue = {
    userid: 1
  };
}

describe('AdminGuardService', () => {
  let authenticationService: AuthenticationService;
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      { provide: Router, useClass: MockRouter },
      { provide: AuthenticationService, useClass: MockAuthenticationService },
    ]
  }));

  beforeEach(() => {
    authenticationService = TestBed.get(AuthenticationService);
  });

  it('should be created', () => {
    const service: AdminGuardService = TestBed.get(AdminGuardService);
    expect(service).toBeTruthy();
  });

});
