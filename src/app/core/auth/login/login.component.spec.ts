import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../authentication.service';
import { By } from '@angular/platform-browser';

class MockFormBuilder {

}

class MockActivatedRoute {
  snapshot = {
    queryParams: {
      returnUrl: '/'
    }
  };
}

class MockRouter {
  navigate() {
  }
}

class MockAuthenticationService {
  currentUserValue = 'admin';
}

describe('LoginComponent', () => {
  let component: LoginComponent;
  let router: Router;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [LoginComponent],
      providers: [FormBuilder,
        { provide: ActivatedRoute, useClass: MockActivatedRoute },
        { provide: Router, useClass: MockRouter },
        { provide: AuthenticationService, useClass: MockAuthenticationService },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    router = TestBed.get(Router);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should redirect if user is login', function () {
    spyOn(router, 'navigate').and.callThrough();
      component.ngOnInit();
      fixture.detectChanges();
      expect(router.navigate).toHaveBeenCalled();
  });

});
