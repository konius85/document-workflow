import { TestBed } from '@angular/core/testing';

import { AuthGuardService } from './auth-guard.service';
import { Router } from '@angular/router';
import { AuthenticationService } from './authentication.service';

class MockRouter {

}

class MockAuthenticationService {

}

describe('AuthGuardService', () => {
  let router: Router;

  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      { provide: Router, useClass: MockRouter },
      { provide: AuthenticationService, useClass: MockAuthenticationService },
    ]
  }));


  it('should be created', () => {
    const service: AuthGuardService = TestBed.get(AuthGuardService);
    expect(service).toBeTruthy();
  });
});
