import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {

  constructor(
    private auth: AuthenticationService
  ) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const url = new URL(req.url);
    if (url.port === '9200') {
      return next.handle(req);
    }
    if (!req.url.endsWith('authenticate')) {
      const userId = this.auth.userId;
      req = req.clone({
        setHeaders: {
          Authorization: `Bearer ${ userId }`
        }
      });
    }

    return next.handle(req);
  }
}
