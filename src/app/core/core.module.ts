import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomMaterialModule } from '../shared/custom-material/custom-material.module';
import { TopBarComponent } from './top-bar/top-bar.component';
import { AuthModule } from './auth/auth.module';

@NgModule({
  declarations: [
    TopBarComponent
  ],
  imports: [
    CommonModule,
    CustomMaterialModule,
    AuthModule,
  ],
  exports: [
    TopBarComponent,
    AuthModule,
  ]
})
export class CoreModule {

  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only');
    }
  }

}
