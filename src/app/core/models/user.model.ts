export interface UserModel {
  userId: number;
  firstName: string;
  lastName: string;
  fullName: string;
}
