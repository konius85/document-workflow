import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopBarComponent } from './top-bar.component';
import { NotificationService } from '../default-providers/notyfication/notification.service';
import { AuthenticationService } from '../auth/authentication.service';
import { Router } from '@angular/router';
import { ElementRef } from '@angular/core';
import { of } from 'rxjs';
import { By } from '@angular/platform-browser';
import { EDocumentModel } from '../default-providers/models/e-document.model';

class MockNotificationService {
  getNotifications() {
    return of({
      hits: [{
        _source: 'test', _id: 'testId', _source: {
          action: 1,
          creationDate: '2019-02-27T17:26:59.240Z',
          documentId: 331,
          fullName: '',
          id: 'cZD-L2kBFbyrD-jIxECX',
          name: 'Opisz szczegóły działania aplikacji',
          taskId: null,
          userId: 1
        }
      }], totals: 1
    });
  }

  deleteNotification() {
    return of([]);
  }
}

class MockAuthenticationService {

}

class MockRouter {

}

class MockElementRef {
  constructor() {
  }
}

describe('TopBarComponent', () => {
  let component: TopBarComponent;
  let notificationService: NotificationService;
  let fixture: ComponentFixture<TopBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TopBarComponent],
      providers: [
        { provide: NotificationService, useClass: MockNotificationService },
        { provide: AuthenticationService, useClass: MockAuthenticationService },
        { provide: Router, useClass: MockRouter },
        { provide: ElementRef, useClass: MockElementRef },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopBarComponent);
    component = fixture.componentInstance;
    notificationService = TestBed.get(NotificationService);
    const store = { currentUser: 'testuser' };
    spyOn(localStorage, 'getItem').and.callFake(function (key) {
      return JSON.stringify(store[key]);
    });
    spyOn(localStorage, 'setItem').and.callFake(function (key, value) {
      return store[key] = value + '';
    });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should remove notification after click on icon', function () {
    component.showNotificationBar = true;
    component.notifications = [{
      id: '1',
      name: 'testName',
      documentId: 1,
    }, {
      id: '2',
      name: 'testName',
      documentId: 2,
    }] as Array<EDocumentModel>;
    fixture.detectChanges();
    const elem = fixture.debugElement.query(By.css('.delete')).triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.notifications.length).toEqual(1);
  });

  it('should show empty notification list after click on icon', function () {
    fixture.debugElement.query(By.css('.fa-bell')).triggerEventHandler('click', null);
    fixture.detectChanges();
    const notification = fixture.debugElement.query(By.css('.notification'));
    expect(notification).toBeTruthy();
  });
});
