import { Component, ElementRef, HostListener, OnInit } from '@angular/core';
import { NotificationService } from '../default-providers/notyfication/notification.service';
import { AuthenticationService } from '../auth/authentication.service';
import { EDocumentModel } from '../default-providers/models/e-document.model';
import { DocumentAction } from '../default-providers/models/document-action.model';
import { Router } from '@angular/router';
import { interval } from 'rxjs';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss']
})
export class TopBarComponent implements OnInit {

  total: number;
  showNotificationBar = false;
  showSettingsMenu = false;
  notifications: Array<EDocumentModel> = [];
  fullName = '';
  admin;

  constructor(
    private notificationService: NotificationService,
    private auth: AuthenticationService,
    private eRef: ElementRef,
    private router: Router,
  ) {
  }

  ngOnInit() {
    this.getNotification();
    interval(1000 * 60).subscribe(_ => this.getNotification());
    this.getUserName();
    this.admin = this.isAdmin();
  }

  isAdmin() {
    return this.auth.userId === 1;
  }

  getUserName() {
    const user = JSON.parse(localStorage.getItem('currentUser'));
    this.fullName = user['fullname'];
  }

  getNotification() {
    this.notificationService.getNotifications()
      .subscribe(data => {
        this.total = data['total'];
        this.notifications = data['hits'].map(hit => {
          const notification = hit['_source'];
          notification.id = hit['_id'];
          return notification;
        });
      });
  }

  showSettings() {
    this.showSettingsMenu = !this.showSettingsMenu;
  }

  logout() {
    this.auth.logout();
  }

  showProfileSettings() {
    console.log('SHOW PROFILE SETTINGS');
  }

  showNotification() {
    this.showNotificationBar = !this.showNotificationBar;
  }

  @HostListener('document:click', ['$event'])
  onDocumentClick(event) {
    if (!this.eRef.nativeElement.contains(event.target)) {
      this.showSettingsMenu = false;
      this.showNotificationBar = false;
    }
  }

  mapNotification(notification: EDocumentModel) {
    switch (notification.action) {
      case DocumentAction.AddDocument:
        return 'Dodano dokument';
      case DocumentAction.EditDocument:
        return 'Edytowano dokument';
      case DocumentAction.RemoveDocument:
        return 'Usunieto dokument';
      case DocumentAction.AddTask:
        return 'Dodano zadanie';
      case DocumentAction.EditTask:
        return 'Edytowano zadanie';
      case DocumentAction.RemoveTask:
        return 'Usunieto zadanie';
      default:
        return 'Brak akcji';
    }
  }

  navigateToLink(notification: EDocumentModel) {
    switch (notification.action) {
      case DocumentAction.AddDocument:
      case DocumentAction.EditDocument:
      case DocumentAction.RemoveDocument:
        this.router.navigate([`/documents/edit/${ notification.documentId }`]);
        break;
      case DocumentAction.AddTask:
      case DocumentAction.EditTask:
      case DocumentAction.RemoveTask:
        this.router.navigate([`tasks/edit/${ notification.taskId }`]);
        break;
      default:
    }
  }

  removeNotification(documentId: string, index) {
    this.notificationService.deleteNotification(documentId)
      .subscribe(_ => {
        this.notifications = this.notifications.filter(notification => notification.id !== documentId);
        this.total = this.total > 0 ? (this.total - 1) : 0;
      });
    // const elemenet = document.getElementsByClassName('list-of-notification')[index];
    // elemenet.classList.add('remove-animation');

  }

  addUser() {
    this.router.navigate([`users`]);
  }

  modifyProfile() {
    this.router.navigateByUrl(`/editprofile`);
  }
}
