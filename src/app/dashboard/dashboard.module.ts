import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { TaskWidgetComponent } from './task-widget/task-widget.component';
import { SharedModule } from '../shared/shared.module';
import { DocumentWidgetComponent } from './document-widget/document-widget.component';
import { CalendarWidgetComponent } from './calendar-widget/calendar-widget.component';
import { CalendarTasksComponent } from './calendar-widget/calendar-tasks/calendar-tasks.component';

@NgModule({
  declarations: [DashboardComponent, TaskWidgetComponent, DocumentWidgetComponent, CalendarWidgetComponent, CalendarTasksComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedModule,
  ],
  exports: [
    DashboardRoutingModule,
  ]
})
export class DashboardModule { }
