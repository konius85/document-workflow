import { Component, OnInit } from '@angular/core';
import { ColumnType } from '../../shared/table/models/column-type.enum';
import { TableService } from '../../shared/table/table.service';
import { DocumentProviderService } from '../../core/default-providers/document/document-provider.service';

@Component({
  selector: 'app-document-widget',
  templateUrl: './document-widget.component.html',
  styleUrls: ['./document-widget.component.scss']
})
export class DocumentWidgetComponent implements OnInit {
  documents;

  columnsConfig = [
    { label: 'Nazwa dokumentu', name: 'name', columnType: ColumnType.Text },
    { label: 'Opis', name: 'description', columnType: ColumnType.Text },
    { label: 'Data dodania', name: 'adddat', columnType: ColumnType.Date },
    { label: 'Data ostatniej modyfikacji', name: 'lastup', columnType: ColumnType.Date },
    { label: 'Utworzył', name: 'user__', columnType: ColumnType.Text },
  ];
  public loading = true;

  constructor(
    private tableService: TableService,
    private documentProviderService: DocumentProviderService
  ) {
  }

  ngOnInit() {
    this.getDocuments();
  }

  getDocuments() {
    return this.documentProviderService.getDocuments()
      .subscribe(documents => {
        this.documents = documents;
        this.loading = false;
        this.setData(documents.splice(0, 4));
      });
  }


  public setData(data: Array<any>) {
    this.tableService.setData({
      columnsConfig: this.columnsConfig,
      data: data
    });
  }

}
