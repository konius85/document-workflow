import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentWidgetComponent } from './document-widget.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TableService } from '../../shared/table/table.service';
import { DocumentProviderService } from '../../core/default-providers/document/document-provider.service';
import { of } from 'rxjs';
import { By } from '@angular/platform-browser';

class MockTableService {
  setData(){
  }
}

class MockDocumentProviderService {
  getDocuments() {
    return of([]);
  }
}

describe('DocumentWidgetComponent', () => {
  let component: DocumentWidgetComponent;
  let documentProviderService: DocumentProviderService;
  let tableService: TableService;
  let fixture: ComponentFixture<DocumentWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DocumentWidgetComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        { provide: TableService, useClass: MockTableService },
        { provide: DocumentProviderService, useClass: MockDocumentProviderService },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentWidgetComponent);
    documentProviderService = TestBed.get(DocumentProviderService);
    tableService = TestBed.get(TableService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show button if documents are set', function () {
    spyOn(documentProviderService, 'getDocuments').and.returnValue(of([]));
    component.ngOnInit();
    fixture.detectChanges();
    const element = fixture.debugElement.query(By.css('button')).nativeElement;
    expect(element.textContent).toContain('Pokaż wszystkie');
  });

  it('should invoke setData method after init', function () {
    spyOn(documentProviderService, 'getDocuments').and.returnValue(of([]));
    spyOn(tableService, 'setData').and.callThrough();
    component.ngOnInit();
    fixture.detectChanges();
    expect(tableService.setData).toHaveBeenCalled();
  });

  it('should show spinner when data are loading', function () {
    component.loading = true;
    fixture.detectChanges();
    const element = fixture.debugElement.query(By.css('.spinner')).nativeElement;
    expect(element).toBeTruthy();
  });
});
