import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendarWidgetComponent } from './calendar-widget.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TaskProviderService } from '../../core/default-providers/task/task-provider.service';
import { By } from '@angular/platform-browser';

class MockTaskProviderService {

}

describe('CalendarWidgetComponent', () => {
  let component: CalendarWidgetComponent;
  let fixture: ComponentFixture<CalendarWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [CalendarWidgetComponent],
      providers: [
        { provide: TaskProviderService, useClass: MockTaskProviderService }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should sets property eventDate ', () => {
    component.eventDate = new Date().toISOString();
    fixture.detectChanges();
    const elem = fixture.debugElement.query(By.css('.event-date')).nativeElement;
    expect(elem.textContent).toContain('Mar 7, 2019');
  });
});
