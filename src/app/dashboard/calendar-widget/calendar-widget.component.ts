import { Component, OnInit } from '@angular/core';
import { TaskProviderService } from '../../core/default-providers/task/task-provider.service';

@Component({
  selector: 'app-calendar-widget',
  templateUrl: './calendar-widget.component.html',
  styleUrls: ['./calendar-widget.component.scss']
})
export class CalendarWidgetComponent implements OnInit {
  tasks = [];
  eventDate;

  constructor(
    private tasksProviderService: TaskProviderService
  ) { }

  ngOnInit() {
  }

  setTasksForActiveDay($event: string) {
   this.eventDate = $event;
    this.tasksProviderService.getTasksForDate($event).subscribe(tasks => {
      this.tasks = tasks['tasks'];
    });
  }
}
