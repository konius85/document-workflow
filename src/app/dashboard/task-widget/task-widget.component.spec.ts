import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskWidgetComponent } from './task-widget.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TaskProviderService } from '../../core/default-providers/task/task-provider.service';
import { of } from 'rxjs';
import { By } from '@angular/platform-browser';

class MockTaskProviderService {
  getTodayTask() {
    return of([])
  }
}

describe('TaskWidgetComponent', () => {
  let component: TaskWidgetComponent;
  let taskProviderService: TaskProviderService;
  let fixture: ComponentFixture<TaskWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [TaskWidgetComponent],
      providers: [
        { provide: TaskProviderService, useClass: MockTaskProviderService },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskWidgetComponent);
    taskProviderService = TestBed.get(TaskProviderService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show today task after init', function () {
    spyOn(taskProviderService, 'getTodayTask').and.returnValue(of([]));
    component.ngOnInit();
    fixture.detectChanges();
    const element = fixture.debugElement.query(By.css('app-task-short-list ')).nativeElement;
    expect(element).toBeTruthy();
  });
});
