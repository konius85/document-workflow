import { Component, OnInit } from '@angular/core';
import { TaskProviderService } from '../../core/default-providers/task/task-provider.service';

@Component({
  selector: 'app-task-widget',
  templateUrl: './task-widget.component.html',
  styleUrls: ['./task-widget.component.scss']
})
export class TaskWidgetComponent implements OnInit {

  tasksToday = [];
  constructor(
    private taskProviderService: TaskProviderService,
  ) { }

  ngOnInit() {
    this.getTodayTask();
  }

  getTodayTask() {
    this.taskProviderService.getTodayTask().subscribe(todayTasks => {
      this.tasksToday = todayTasks;
    });
  }
}
