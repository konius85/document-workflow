import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsersComponent } from './users/users.component';
import { UserProfileComponent } from '../shared/user-profile/user-profile.component';
import { AdminGuardService } from '../core/auth/admin-guard.service';
import { EditProfileComponent } from './users/edit-profile/edit-profile.component';

const routes: Routes = [
  { path: '', component: UsersComponent },
  { path: 'editprofile', component: EditProfileComponent },
  { path: 'edit/:id', component: UserProfileComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule {
}

