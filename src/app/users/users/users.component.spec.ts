import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersComponent } from './users.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TableService } from '../../shared/table/table.service';
import { UsersProviderService } from '../../core/default-providers/users/users-provider.service';
import { Router } from '@angular/router';
import { of, ReplaySubject, Subject } from 'rxjs';
import { ActionType } from '../../shared/table/models/action-type.model';
import { DataTable } from '../../shared/table/models/data-table.model';

class MockTableService {
  public selectedAction$: Subject<{ action: ActionType, rowData: number }> = new Subject();
  public dataTable$: ReplaySubject<DataTable> = new ReplaySubject(1);

  setData() {

  }
}

class MockUsersProviderService {
  getUsers() {
    return of([]);
  }
}

class MockRouter {

}

describe('UsersComponent', () => {
  let component: UsersComponent;
  let fixture: ComponentFixture<UsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [UsersComponent],
      providers: [
        { provide: TableService, useClass: MockTableService },
        { provide: UsersProviderService, useClass: MockUsersProviderService },
        { provide: Router, useClass: MockRouter },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
