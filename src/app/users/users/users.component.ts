import { Component, OnDestroy, OnInit } from '@angular/core';
import { ColumnType } from '../../shared/table/models/column-type.enum';
import { TableService } from '../../shared/table/table.service';
import { UsersProviderService } from '../../core/default-providers/users/users-provider.service';
import { map, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { ActionType } from 'src/app/shared/table/models/action-type.model';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit, OnDestroy {

  unSubscribe$: Subject<boolean> = new Subject();

  columnsConfig = [
    { label: 'Id', name: 'userid', columnType: ColumnType.Text },
    { label: 'Imie', name: 'firstname', columnType: ColumnType.Text },
    { label: 'Nazwisko', name: 'lastname', columnType: ColumnType.Text },
    { label: 'Login', name: 'login', columnType: ColumnType.Text },
    { label: 'Typ konta', name: 'rolename', columnType: ColumnType.Text },
    { label: 'Akcje', name: 'uid', columnType: ColumnType.Action },
  ];

  constructor(
    private tableService: TableService,
    private usersProviderService: UsersProviderService,
    private router: Router,
  ) {
  }

  ngOnInit() {
    this.getUsers();
    this.observerSelectedAction();
  }

  ngOnDestroy(): void {
    this.unSubscribe$.next();
    this.unSubscribe$.complete();
  }

  getUsers() {
    return this.usersProviderService.getUsers()
      .pipe(map(users => users['users'])).subscribe(data => {
        this.setData(data);
      });
  }

  setData(data: Array<any>) {
    this.tableService.setData({
      columnsConfig: this.columnsConfig,
      data: data
    });
  }

  observerSelectedAction() {
    this.tableService.selectedAction$.pipe(
      takeUntil(this.unSubscribe$))
      .subscribe(selectedAction => {
        switch (selectedAction.action) {
          case ActionType.Redirect:
            this.router.navigate(['/users/edit', selectedAction.rowData]);
            break;
          case ActionType.Remove:
            break;
        }
      });
  }

}
