import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './users/users.component';
import { UsersRoutingModule } from './users-routing.module';
import { SharedModule } from '../shared/shared.module';
import { EditUserComponent } from './users/edit-user/edit-user.component';
import { EditProfileComponent } from './users/edit-profile/edit-profile.component';

@NgModule({
  declarations: [UsersComponent, EditUserComponent, EditProfileComponent],
  imports: [
    CommonModule,
    UsersRoutingModule,
    SharedModule,
  ],
  exports: [
    UsersComponent,
    EditProfileComponent
  ]
})
export class UsersModule { }
