export interface DocumentModel {
  documentId: number;
  name: string;
  docName?: string;
  userId?: number;
  description: string;
  createDate: string;
  lastUpdate: string;
  addUserId: number;
  documentTypeId: number;
  fileId?: number;
  modificationUserId?: number;
  statusId?: number;
  fileName: string;
  fileCreated: string;
}
