import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentsComponent } from './documents.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { DocumentsService } from './documents.service';
import { TableService } from '../shared/table/table.service';
import { Router } from '@angular/router';
import { of, ReplaySubject, Subject } from 'rxjs';
import { ActionType } from '../shared/table/models/action-type.model';
import { DataTable } from '../shared/table/models/data-table.model';

class MockDocumentsService {
  getDocuments() {
    return of([]);
  }

  setData() {
  }

  getDocumentName() {
    return of([])
  }

  removeDocument() {
    return of([]);
  }
}

class MockTableService {
  public selectedAction$: Subject<{action: ActionType, rowData: number}> = new Subject();
  public dataTable$: ReplaySubject<DataTable> = new ReplaySubject(1);

}

class MockRouter {
  navigate() {
  }
}

describe('DocumentsComponent', () => {
  let component: DocumentsComponent;
  let router: Router;
  let documentsService: DocumentsService;
  let tableService: TableService;
  let fixture: ComponentFixture<DocumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [DocumentsComponent],
      providers: [
        { provide: DocumentsService, useClass: MockDocumentsService },
        { provide: TableService, useClass: MockTableService },
        { provide: Router, useClass: MockRouter },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentsComponent);
    documentsService = TestBed.get(DocumentsService);
    tableService = TestBed.get(TableService);
    router = TestBed.get(Router);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set data to the table component', function () {
    spyOn(documentsService, 'getDocuments').and.callThrough();
    spyOn(documentsService, 'setData').and.callThrough();
    component.ngOnInit();
    fixture.detectChanges();
    expect(documentsService.setData).toHaveBeenCalled();
  });

  it('should search documents when receive new data',  async() => {
    spyOn(documentsService, 'getDocumentName').and.callFake( () => of(['testData']));
    spyOn(documentsService, 'setData').and.callFake( () => of('testData'));
    component.ngOnInit();
    fixture.detectChanges();
    component.searchTerm$.subscribe(data => {
      expect(data).toContain('testData');
    });
    component.searchTerm$.next('testData');
  });

  it('should redirect when component emits redirect action', function () {
    spyOn(router, 'navigate').and.callThrough();
    tableService.selectedAction$.subscribe(data => {
      expect(router.navigate).toHaveBeenCalled();
    });
    component.ngOnInit();
    tableService.selectedAction$.next({action : ActionType.Redirect, rowData: 12});
    fixture.detectChanges();

  });

  it('should remove row when component emits `remove` action', function () {
    spyOn(documentsService, 'removeDocument').and.callThrough();
    tableService.selectedAction$.subscribe(data => {
      expect(documentsService.removeDocument).toHaveBeenCalledWith('12');
    });
    component.ngOnInit();
    tableService.selectedAction$.next({action : ActionType.Remove, rowData: 12});
    fixture.detectChanges();

  });
});
