import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDocumentComponent } from './edit-document.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { DocumentProviderService } from '../../core/default-providers/document/document-provider.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Observable, of } from 'rxjs';

class MockDocumentProviderService {
  getDocumentById() {
    return of([]);
  }
}

class MockActivatedRoute {

}

describe('EditDocumentComponent', () => {
  let component: EditDocumentComponent;
  let fixture: ComponentFixture<EditDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [EditDocumentComponent],
      providers: [
        { provide: DocumentProviderService, useClass: MockDocumentProviderService },
        { provide: ActivatedRoute, useValue: { params: of({ id: 123 }) } },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
