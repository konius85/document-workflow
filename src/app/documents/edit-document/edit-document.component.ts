import { Component, OnInit } from '@angular/core';
import { DocumentProviderService } from '../../core/default-providers/document/document-provider.service';
import { ActivatedRoute } from '@angular/router';
import { DocumentModel } from '../models/document.model';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-edit-document',
  templateUrl: './edit-document.component.html',
  styleUrls: ['./edit-document.component.scss']
})
export class EditDocumentComponent implements OnInit {

  documentData: DocumentModel;

  constructor(
    private documentProviderService: DocumentProviderService,
    private activatedRoute: ActivatedRoute
  ) {
  }

  ngOnInit() {
    this.getDocumentData();
  }

  getDocumentData() {
    console.log('test', this.activatedRoute)
    this.activatedRoute.params
      .pipe(switchMap(data => this.documentProviderService.getDocumentById(data['id'])))
      .subscribe(data1 => {
        this.documentData = data1;
      });
  }

}
