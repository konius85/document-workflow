import { TestBed } from '@angular/core/testing';

import { DocumentsService } from './documents.service';
import { TableService } from '../shared/table/table.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

class MockTableService {

}

describe('DocumentsService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [
      { provide: TableService, useClass: MockTableService },
    ]
  }));

  it('should be created', () => {
    const service: DocumentsService = TestBed.get(DocumentsService);
    expect(service).toBeTruthy();
  });
});
