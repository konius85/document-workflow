import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DocumentsComponent } from './documents.component';
import { AddDocumentComponent } from './add-document/add-document.component';
import { EditDocumentComponent } from './edit-document/edit-document.component';


const routes: Routes = [
  { path: '',  component: DocumentsComponent },
  { path: 'add',  component: AddDocumentComponent },
  { path: 'edit/:id',  component: EditDocumentComponent },
  { path: 'remove',  component: AddDocumentComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]

})
export class DocumentsRoutingModule { }
