import { Injectable } from '@angular/core';
import { TableService } from '../shared/table/table.service';
import { ColumnType } from '../shared/table/models/column-type.enum';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DocumentsService {

  columnsConfig = [
    { label: 'Nazwa dokumentu', name: 'name', columnType: ColumnType.Text },
    { label: 'Opis', name: 'description', columnType: ColumnType.Text },
    { label: 'Data dodania', name: 'adddat', columnType: ColumnType.Date },
    { label: 'Data ostatniej modyfikacji', name: 'lastup', columnType: ColumnType.Date },
    { label: 'Utworzył', name: 'user__', columnType: ColumnType.Text },
    { label: 'Status', name: 'status', columnType: ColumnType.Status },
    { label: 'Typ', name: 'type__', columnType: ColumnType.Text },
    { label: 'Plik', name: 'file', columnType: ColumnType.File },
    { label: 'Akcje', name: 'documentid', columnType: ColumnType.Action },
  ];

  constructor(
    private tableService: TableService,
    private http: HttpClient
  ) {
  }

  /**
   * Method gets all documents
   * @returns Observable<any>
   */
  public getDocuments(): Observable<any> {
    return this.http.get('http://localhost/v1/documents')
      .pipe(map(data => data['data']));
  }

  /**
   * Method gets documents by name
   * @param name
   * @returns Observable<any>
   */
  public getDocumentName(name: string): Observable<any> {
    return this.http.get(`http://localhost/v1/documents?q=${ name }`)
      .pipe(map(data => data['data']));
  }

  /**
   * Method sets data for table
   * @param data {Array}
   */
  public setData(data: Array<any>) {
    this.tableService.setData({
      columnsConfig: this.columnsConfig,
      data: data
    });
  }


  save(data) {
    return this.http.post('http://localhost/v1/documents/add', { name: data.name })
      .subscribe(data1 => console.log(data1));
  }

  getDocumentStatus(documentType: number) {
    return this.http.post('http://localhost/v1/status', { documentType: documentType });
  }

  removeDocument(docId: string): Observable<any> {
    return this.http.post('http://localhost/v1/documents/removeDocument', { docId });
  }
}

