import { Component, OnDestroy, OnInit } from '@angular/core';
import { DocumentsService } from './documents.service';
import { debounceTime, distinctUntilChanged, switchMap, takeUntil, tap } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { TableService } from '../shared/table/table.service';
import { ActionType } from '../shared/table/models/action-type.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.scss']
})
export class DocumentsComponent implements OnInit, OnDestroy {

  searchTerm$: Subject<string> = new Subject<string>();
  private unSubscribe$: Subject<any> = new Subject();
  loading = true;

  constructor(
    private documentsService: DocumentsService,
    private tableService: TableService,
    private router: Router,
  ) {
  }

  ngOnInit() {
    this.documentsService.getDocuments()
      .pipe(
        takeUntil(this.unSubscribe$),
        tap(_ => this.loading = false)
      )
      .subscribe(data => {
        this.documentsService.setData(data);
      });
    this.search();
    this.observeSelectedAction();
  }

  ngOnDestroy(): void {
    this.unSubscribe$.next();
    this.unSubscribe$.complete();
  }


  search() {
    this.searchTerm$.pipe(
      debounceTime(500),
      distinctUntilChanged(),
      switchMap(value => this.documentsService.getDocumentName(value))
    ).subscribe(data => this.documentsService.setData(data));
  }

  onSearch(value: string) {
    this.searchTerm$.next(value);
  }

  observeSelectedAction() {
    this.tableService.selectedAction$.pipe(
      takeUntil(this.unSubscribe$),
    ).subscribe(selectedAction => {
      switch (selectedAction.action) {
        case ActionType.Redirect:
          this.router.navigate(['/documents/edit', selectedAction.rowData]);
          break;
        case  ActionType.Remove:
          this.removeDocument(selectedAction.rowData.toString());
          break;
      }
    });
  }

  removeDocument(docId: string) {
    this.documentsService.removeDocument(docId).subscribe();
  }
}
