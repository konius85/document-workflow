import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DocumentsComponent } from './documents.component';
import { DocumentsRoutingModule } from './documents-routing.module';
import { SharedModule } from '../shared/shared.module';
import { AddDocumentComponent } from './add-document/add-document.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EditDocumentComponent } from './edit-document/edit-document.component';

@NgModule({
  declarations: [
    DocumentsComponent,
    AddDocumentComponent,
    EditDocumentComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DocumentsRoutingModule,
    SharedModule
  ],
  exports: [
    DocumentsRoutingModule,
    DocumentsComponent,
  ],
})
export class DocumentsModule { }
