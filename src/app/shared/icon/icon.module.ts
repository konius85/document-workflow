import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PdfIconComponent } from './pdf-icon/pdf-icon.component';
import { IconFactoryComponent } from './icon-factory/icon-factory.component';
import { DocIconComponent } from './doc-icon/doc-icon.component';
import { UndefinedIconComponent } from './undefined-icon/undefined-icon.component';
import { ZipIconComponent } from './zip-icon/zip-icon.component';


const icons = [
  PdfIconComponent,
  DocIconComponent,
  UndefinedIconComponent,
  ZipIconComponent,
];

@NgModule({
  declarations: [
    ...icons,
    IconFactoryComponent,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ...icons,
    IconFactoryComponent
  ],
  entryComponents: [
    icons,
  ]
})
export class IconModule { }
