import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IconFactoryComponent } from './icon-factory.component';

describe('IconFactoryComponent', () => {
  let component: IconFactoryComponent;
  let fixture: ComponentFixture<IconFactoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IconFactoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconFactoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
