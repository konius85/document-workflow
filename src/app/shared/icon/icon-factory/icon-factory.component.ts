import { Component, ComponentFactoryResolver, Input, OnInit, ViewContainerRef } from '@angular/core';
import { IconService } from '../icon.service';
import { IconType } from '../icon-type.enum';


@Component({
  selector: 'app-icon-factory',
  template: ``,
})
export class IconFactoryComponent implements OnInit {

  @Input() icon: IconType;
  @Input() cssClass;

  constructor(
    private iconService: IconService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private vcRef: ViewContainerRef
  ) {
  }

  ngOnInit() {
    if (this.icon) {
      const { component } = this.iconService.getIconType(this.icon);
      const componentFactory = this.componentFactoryResolver.resolveComponentFactory(component);
      this.vcRef.createComponent(componentFactory);
    }
  }
}
