import { Injectable } from '@angular/core';
import { IconType } from './icon-type.enum';
import { UndefinedIconComponent } from './undefined-icon/undefined-icon.component';
import { PdfIconComponent } from './pdf-icon/pdf-icon.component';
import { DocIconComponent } from './doc-icon/doc-icon.component';
import { ZipIconComponent } from './zip-icon/zip-icon.component';

@Injectable({
  providedIn: 'root'
})
export class IconService {

  private defaultIcon = {extension: IconType.Unknown, component: UndefinedIconComponent};

  private iconTypes = [
    {extension: IconType.Pdf, component: PdfIconComponent},
    {extension: IconType.Doc, component: DocIconComponent},
    {extension: IconType.Docx, component: DocIconComponent},
    {extension: IconType.Zip, component: ZipIconComponent},
  ];


  public getIconType(type: IconType) {
    const data = this.iconTypes.find(iconType => iconType.extension === type);
    if (!data) {
      return this.defaultIcon;
    }
    return data;
  }
}
