import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZipIconComponent } from './zip-icon.component';

describe('ZipIconComponent', () => {
  let component: ZipIconComponent;
  let fixture: ComponentFixture<ZipIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZipIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZipIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
