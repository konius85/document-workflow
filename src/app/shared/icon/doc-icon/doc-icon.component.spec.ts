import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocIconComponent } from './doc-icon.component';

describe('DocIconComponent', () => {
  let component: DocIconComponent;
  let fixture: ComponentFixture<DocIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
