import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-undefined-icon',
  templateUrl: './undefined-icon.component.html',
  styleUrls: ['./undefined-icon.component.scss']
})
export class UndefinedIconComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
