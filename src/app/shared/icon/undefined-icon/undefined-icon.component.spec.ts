import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UndefinedIconComponent } from './undefined-icon.component';

describe('UndefinedIconComponent', () => {
  let component: UndefinedIconComponent;
  let fixture: ComponentFixture<UndefinedIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UndefinedIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UndefinedIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
