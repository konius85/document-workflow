export enum IconType {
  Pdf = 'pdf',
  Doc = 'doc',
  Docx = 'docx',
  Zip = 'zip',
  Unknown = 'unknown',
}
