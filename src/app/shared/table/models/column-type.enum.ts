export enum ColumnType {
  Text,
  Date,
  Action,
  Status,
  File,
  Icon,
}
