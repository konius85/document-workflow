export enum ActionType {
  Edit = 'edit',
  Redirect = 'redirect',
  Remove = 'remove',
}
