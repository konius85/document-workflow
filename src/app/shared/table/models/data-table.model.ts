export interface ColumnConfig {
  label: string;
  name: string;
  columnType: any;
}

export interface DataTable {
  columnsConfig: Array<ColumnConfig>;
  data: Array<any>;
}


