import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DocumentViewerComponent } from '../../../document-viewer/document-viewer.component';

@Component({
  selector: 'app-file-cell',
  templateUrl: './file-cell.component.html',
  styleUrls: ['./file-cell.component.scss']
})
export class FileCellComponent implements OnInit {

  @Input() rowData;
  cellData;
  constructor(public dialog: MatDialog) { }

  ngOnInit() {
    this.cellData = this.rowData.extension ? this.rowData.extension.trim() : null;
  }

  showFile() {
    const dialogRef = this.dialog.open(DocumentViewerComponent, {
      data: {rowData: this.rowData}, width: '700'
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }
}
