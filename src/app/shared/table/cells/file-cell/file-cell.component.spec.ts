import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileCellComponent } from './file-cell.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { MatDialogModule } from '@angular/material';

describe('FileCellComponent', () => {
  let component: FileCellComponent;
  let fixture: ComponentFixture<FileCellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MatDialogModule],
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [FileCellComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileCellComponent);
    component = fixture.componentInstance;
    component.rowData = { extension: 'pdf' };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
