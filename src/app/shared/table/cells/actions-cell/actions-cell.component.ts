import { Component, Input, OnInit } from '@angular/core';
import { ActionType } from '../../models/action-type.model';
import { TableService } from '../../table.service';

@Component({
  selector: 'app-actions-cell',
  templateUrl: './actions-cell.component.html',
  styleUrls: ['./actions-cell.component.scss']
})
export class ActionsCellComponent implements OnInit {
  actionType = ActionType;
  @Input() rowData: number;

  constructor(
    private tableService: TableService,
  ) {
  }

  ngOnInit() {
  }

  emmitAction(action: ActionType, rowData: number) {
    this.tableService.selectedAction$.next({ action, rowData });
  }

  redirect() {
    this.tableService.selectedAction$.next({ action: ActionType.Redirect, rowData: this.rowData });
  }
}
