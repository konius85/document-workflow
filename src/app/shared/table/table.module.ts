import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableComponent } from './table.component';
import { CustomMaterialModule } from '../custom-material/custom-material.module';
import { TableFilterComponent } from './table-filter/table-filter.component';
import { TableCellComponent } from './table-cell/table-cell.component';
import { TextCellComponent } from './cells/text-cell/text-cell.component';
import { DateCellComponent } from './cells/date-cell/date-cell.component';
import { ActionsCellComponent } from './cells/actions-cell/actions-cell.component';
import { StatusCellComponent } from './cells/status-cell/status-cell.component';
import { FileCellComponent } from './cells/file-cell/file-cell.component';
import { IconModule } from '../icon/icon.module';
import { IconCellComponent } from './cells/icon-cell/icon-cell.component';

@NgModule({
  declarations: [
    TableComponent,
    TableFilterComponent,
    TableCellComponent,
    TextCellComponent,
    DateCellComponent,
    ActionsCellComponent,
    StatusCellComponent,
    FileCellComponent,
    IconCellComponent,
  ],
  imports: [
    CommonModule,
    CustomMaterialModule,
    IconModule,
  ],
  exports: [
    TableComponent,
    TableFilterComponent,
    TextCellComponent,
    ActionsCellComponent,
    StatusCellComponent,
    FileCellComponent,
    IconModule,
  ],
  entryComponents: [
    TableCellComponent,
    TextCellComponent,
    DateCellComponent,
    ActionsCellComponent,
    StatusCellComponent,
    FileCellComponent,
    IconCellComponent,
  ]
})
export class TableModule {
}
