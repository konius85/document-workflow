import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableCellComponent } from './table-cell.component';
import { Component, ComponentFactoryResolver, ViewContainerRef } from '@angular/core';
import { TableCellService } from './table-cell.service';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';

@Component({
  selector: 'app-fake-component',
  template: '',
})
class FakeComponent {
}

class MockTableCellService {
  getCellType() {
    return FakeComponent;
  }
}

class MockComponentFactoryResolver {
  resolveComponentFactory() {
    return FakeComponent;
  }

}

class MockViewContainerRef {


  createComponent() {
    const create = function () {

    };
  }
}

describe('TableCellComponent', () => {
  let component: TableCellComponent;
  let fixture: ComponentFixture<TableCellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TableCellComponent, FakeComponent],
      providers: [ComponentFactoryResolver, ViewContainerRef,
        { provide: TableCellService, useClass: MockTableCellService },
      ]
    }),
      TestBed.overrideModule(BrowserDynamicTestingModule, {
        set: {
          entryComponents: [FakeComponent]
        }
      })
        .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


});
