import { Injectable } from '@angular/core';
import { ColumnType } from '../models/column-type.enum';
import { TextCellComponent } from '../cells/text-cell/text-cell.component';
import { CellType } from './cell-type.model';
import { DateCellComponent } from '../cells/date-cell/date-cell.component';
import { ActionsCellComponent } from '../cells/actions-cell/actions-cell.component';
import { StatusCellComponent } from '../cells/status-cell/status-cell.component';
import { FileCellComponent } from '../cells/file-cell/file-cell.component';
import { IconCellComponent } from '../cells/icon-cell/icon-cell.component';

@Injectable({
  providedIn: 'root'
})
export class TableCellService {

  private cellTypes: Array<CellType> = [
    { type: ColumnType.Text,  component: TextCellComponent },
    { type: ColumnType.Date,  component: DateCellComponent },
    { type: ColumnType.Action,  component: ActionsCellComponent },
    { type: ColumnType.Status,  component: StatusCellComponent },
    { type: ColumnType.File,  component: FileCellComponent },
    { type: ColumnType.Icon,  component: IconCellComponent },
  ];

  public getCellType(type) {
    return this.cellTypes.find(cell => cell.type === type);
  }
}
