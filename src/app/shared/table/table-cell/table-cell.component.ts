import { Component, ComponentFactoryResolver, Input, OnInit, ViewContainerRef } from '@angular/core';
import { TableCellService } from './table-cell.service';

@Component({
  selector: 'app-table-cell',
  templateUrl: './table-cell.component.html',
  styleUrls: ['./table-cell.component.scss']
})
export class TableCellComponent implements OnInit {

  @Input() rowData;
  @Input() columnType;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private tableCellService: TableCellService,
    private vcRef: ViewContainerRef) {
  }

  ngOnInit() {
    const { component } = this.tableCellService.getCellType(this.columnType);
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(component);
    const componentRef = this.vcRef.createComponent(componentFactory);
    (<any>componentRef.instance).rowData = this.rowData;
  }

}
