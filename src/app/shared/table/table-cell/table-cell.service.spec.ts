import { TestBed } from '@angular/core/testing';

import { TableCellService } from './table-cell.service';

describe('TableCellService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TableCellService = TestBed.get(TableCellService);
    expect(service).toBeTruthy();
  });
});
