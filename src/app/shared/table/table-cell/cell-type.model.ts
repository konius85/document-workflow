import { ColumnType } from '../models/column-type.enum';

export interface CellType {
  type: ColumnType;
  component: any;
}
