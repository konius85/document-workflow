import { Injectable } from '@angular/core';
import { BehaviorSubject, ReplaySubject, Subject } from 'rxjs';
import { DataTable } from './models/data-table.model';
import { ActionType } from './models/action-type.model';

@Injectable({
  providedIn: 'root'
})
export class TableService {
  public selectedAction$: Subject<{action: ActionType, rowData: number}> = new Subject();
  public dataTable$: ReplaySubject<DataTable> = new ReplaySubject(1);

  /**
   * Method sets data for the table
   * @param data Data table
   */
  public setData(data: DataTable) {
   this.dataTable$.next({
     columnsConfig: data.columnsConfig,
     data: data.data,
   });
  }
}
