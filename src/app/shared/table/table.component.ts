import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { TableService } from './table.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @Input() disabledPaginator = false;
  displayedColumns: string[] = [];
  dataSource: MatTableDataSource<any>;
  columnsConfig = [];
  unSubscribe$: Subject<any> = new Subject();

  constructor(private tableService: TableService) {
  }

  ngOnInit() {
    this._getData();
    this.tableService.selectedAction$.pipe(
      takeUntil(this.unSubscribe$)
    ).subscribe(data => {

      switch (data.action) {
        case 'remove':
          this.removeRow('documentid', data.rowData);
          this.removeRow('taskid', data.rowData);
          break;
      }
    });
  }

  ngOnDestroy(): void {
    this.unSubscribe$.next();
    this.unSubscribe$.complete();
  }

  public removeRow(columnId: string, docId) {
    if (columnId === 'documentid') {
      this.dataSource.data = this.dataSource.data.filter(data => data[columnId] !== docId);
    }
    if (columnId === 'taskid') {
      const result = this.dataSource.data.filter(data => data[columnId] === docId);

      if (result.length) {
        if (!result[0]['iscompleted']) {
          this.dataSource.data = this.dataSource.data.filter(data => data[columnId] !== docId);
        }
      }
    }

  }

  private _getData() {
    this.tableService.dataTable$.pipe(takeUntil(this.unSubscribe$))
      .subscribe(data => {
        this.columnsConfig = data.columnsConfig;
        this.displayedColumns = this._prepareColumns(data.columnsConfig);
        this.dataSource = new MatTableDataSource<any>(data.data);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
  }

  private _prepareColumns(columnsConfig): Array<string> {
    return columnsConfig.map(column => column.name);
  }
}
