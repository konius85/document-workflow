import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ComboBoxPipe } from './combo-box-pipe';


@Component({
  selector: 'app-combobox',
  templateUrl: './combobox.component.html',
  styleUrls: ['./combobox.component.scss'],
  providers: [ComboBoxPipe]
})
export class ComboboxComponent implements OnInit {

  @Input() dataList: [];
  @Input() columnName: string;
  @Input() label: string;
  @Input() initialData;
  @Output() selectedItem: EventEmitter<any> = new EventEmitter();
  dummyDataList: any;
  showDropDown: boolean;
  textToSort;

  setInitalData() {
    // @Todo DEFINE TASK MODEL
    if (this.initialData && this.initialData['docid']) {
      this.initialData.documentId = this.initialData.docid;
      this.initialData.docName = this.initialData.docname;
      this.dummyDataList.push(this.initialData);
      this.updateList(this.initialData);
      this.showDropDown = false;
    }

  }

  onFocusEventAction(): void {
    if (this.dataList) {
      this.showDropDown = true;
    }
    this.reset();
  }

  onFocusOut(): void {
    if (this.dataList) {
      this.showDropDown = false;
    }
  }


  constructor(private comboBoxPipe: ComboBoxPipe) {
    // this.reset();
  }

  ngOnInit() {
    this.reset();
    this.setInitalData();
  }

  toogleDropDown(): void {
    this.showDropDown = !this.showDropDown;
  }

  reset(): void {
    this.textToSort = '';
    this.dataList = [];
    this.dummyDataList = this.dataList;
  }

  textChange(value) {
    this.dummyDataList = [];
    if (value.length > 0) {
      this.dummyDataList = this.comboBoxPipe.transform(this.dataList,
        this.columnName, value);

      if (this.dummyDataList) {
        this.showDropDown = true;
      }
    } else {
      this.reset();
    }
  }


  updateList(data) {
    const { docName, documentId } = this.dummyDataList.find(doc => doc.documentId === data.documentId);
    this.textToSort = docName;
    this.selectedItem.emit(documentId);
    this.toogleDropDown();
  }
}
