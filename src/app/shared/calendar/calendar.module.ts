import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalendarViewComponent } from './calendar-view/calendar-view.component';
import { CalendarService } from './calendar.service';


@NgModule({
  imports: [
    CommonModule,
  ],
  exports: [
    CalendarViewComponent
  ],
  declarations: [
    CalendarViewComponent
  ],
  providers: [
    CalendarService
  ]
})
export class CalendarModule {
}
