import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CalendarService {
  public activeDay$: BehaviorSubject<string> = new BehaviorSubject(new Date().toISOString());
  public eventsDays$: BehaviorSubject<Array<string>> = new BehaviorSubject<Array<string>>([]);
  public firstLastCalendarDay$: Subject<{ firstDay: string, lastDay: string }> = new Subject();

  /**
   * Method sets days for calendar
   * @param firstDay {string} Iso date
   * @param lastDay {string} Iso date
   */
  public setFirstLastCalendarDay(firstDay: string, lastDay: string) {
    this.firstLastCalendarDay$.next({firstDay, lastDay});
  }
}
