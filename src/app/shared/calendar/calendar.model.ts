import { Moment } from 'moment';

export interface Week {
  date: Moment;
  name: string;
  number: number;
  isToday: boolean;
  isCurrentMonth: boolean;
}

export interface Weeks {
  days: Week[];
}

export interface DateFormat {
  year?: string;
  month?: string;
  day?: string;
}
