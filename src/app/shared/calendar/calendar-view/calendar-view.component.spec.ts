import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import * as moment from 'moment';
import { CalendarViewComponent } from './calendar-view.component';
import { By } from '@angular/platform-browser';
import { CalendarService } from '../calendar.service';
import { BehaviorSubject, Subject } from 'rxjs';

class MockCalendarService {
  public activeDay$: BehaviorSubject<string> = new BehaviorSubject(new Date().toISOString());
  public eventsDays$: BehaviorSubject<Array<string>> = new BehaviorSubject<Array<string>>([]);
  public firstLastCalendarDay$: Subject<{ firstDay: string, lastDay: string }> = new Subject();

  setFirstLastCalendarDay() {

  }
}

describe('CalendarViewComponent', () => {
  let component: CalendarViewComponent;
  let calendarService: CalendarService;
  let fixture: ComponentFixture<CalendarViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [],
      declarations: [CalendarViewComponent],
      providers: [
        { provide: CalendarService, useClass: MockCalendarService }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarViewComponent);
    component = fixture.componentInstance;
    calendarService = TestBed.get(CalendarService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call component method to set first and last day in calendar', () => {
    const date = moment();
    const month = moment();
    const setLastFirstDaySpy = spyOn(component, 'setLastFirstDay').and.returnValue([]);
    component.buildMonth(date, month);
    fixture.detectChanges();
    expect(setLastFirstDaySpy).toHaveBeenCalled();
  });

  it('should change month after click on the next button', () => {

    const nextMonth = moment().add(+1, 'month');

    const next = component.formatMonthYear(nextMonth);
    const prevDe = fixture.debugElement.queryAll(By.css('.material-icons'));
    prevDe[1].triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.monthYear).toEqual(next, 'next month');
  });

  it('should change month after click on the prev button', () => {

    const prevMonth = moment().add(-1, 'month');
    const prev = component.formatMonthYear(prevMonth);
    const prevDe = fixture.debugElement.queryAll(By.css('.material-icons'));

    prevDe[0].triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.monthYear).toEqual(prev, 'prev month');
  });

  it('should return first day of week', () => {
    const day = moment('2018-06-20');
    const result = component.removeTime(day);
    const firstDay = result.format('YYYY-MM-DD');
    expect(firstDay).toEqual('2018-06-18');
  });

  // it('should change active date after click', function () {
  //   component.buildCalendar();
  //   fixture.detectChanges();
  //   const dayDe = fixture.debugElement.query(By.css('span.dayNumber')).nativeElement;
  //   const content = parseInt(dayDe.textContent, 10);
  //
  //   dayDe.click();
  //   fixture.detectChanges();
  //   calendarService.activeDay$.subscribe(date =>
  //     expect(content).toEqual(moment(date).date())
  //   );
  // });

  it('should set `event` class to native element', () => {
    component.eventsDays = [new Date().toISOString()];
    component.buildCalendar();
    fixture.detectChanges();
    const days = fixture.debugElement.query(By.css('.event')).nativeElement;
    expect(days.classList).toContain('event');
  });

});
