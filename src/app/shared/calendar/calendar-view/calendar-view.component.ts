import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import * as moment from 'moment';
import { Moment } from 'moment';
import { DateFormat, Week, Weeks } from '../calendar.model';
import { CalendarService } from '../calendar.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar-view.component.html',
  styleUrls: ['./calendar-view.component.scss'],
})
export class CalendarViewComponent implements OnInit, OnDestroy {
  eventsDays: Array<string> = [];
  monthYear: DateFormat;
  activeDay = moment();
  month: Moment;
  firstWeekDay: Moment;
  weeks: Array<Weeks>;
  day = moment().format('D');
  @Output() activeDateEmiter: EventEmitter<string> = new EventEmitter();
  private ngUnsubscribe: Subject<any> = new Subject();

  constructor(private calendarService: CalendarService) {
  }

  ngOnInit() {
    this.buildCalendar();
    this.getEventsDays();
    this.activeDateEmiter.emit(moment(this.activeDay).format('YYYY-MM-DD'));
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  buildCalendar() {
    this.firstWeekDay = this.removeTime(this.firstWeekDay || moment()) ;
    this.monthYear = this.formatMonthYear(this.activeDay);
    this.month = this.activeDay.clone();
    const start = this.activeDay.clone();
    start.date(0);
    this.removeTime(start.day(0));
    this.buildMonth(start, this.month);
  }

  getEventsDays() {
    this.calendarService.eventsDays$.pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(days => {
      this.eventsDays = days;
    });
  }

  buildMonth(start: Moment, month: Moment) {
    this.weeks = [];
    let done = false, date = start.clone(), monthIndex = date.month(), count = 0;
    while (!done) {
      this.weeks.push({days: this.buildWeek(date.clone(), month)});
      date.add(1, 'w');
      done = count++ > 2 && monthIndex !== date.month();
      monthIndex = date.month();
    }
    this.setLastFirstDay();
  }

  setLastFirstDay() {
    const length = this.weeks.length - 1;
    const firstDay = this.weeks[0].days[0].date.toDate().toISOString();
    const lastDay = this.weeks[length].days[6].date.add(1, 'day').toDate().toISOString();
    this.calendarService.setFirstLastCalendarDay(firstDay, lastDay);
  }

  buildWeek(date: Moment, month: Moment): Week[] {
    const days = [];
    for (let i = 0; i < 7; i++) {
      days.push({
        name: date.format('dd').substring(0, 1),
        number: date.date(),
        isCurrentMonth: date.month() === month.month(),
        isToday: date.isSame(moment().format(), 'day'),
        date: date
      });
      date = date.clone();
      date.add(1, 'd');
    }

    return days;
  }

  next() {
    const next = this.month.clone();
    this.removeTime(next.month(next.month() + 1).date(0));
    this.month.month(this.month.month() + 1);
    this.monthYear = this.formatMonthYear(this.month);
    this.buildMonth(next, this.month);
    this.calendarService.activeDay$.next(this.month.toISOString());
  }

  previous() {
    const previous = this.month.clone();
    this.removeTime(previous.month(previous.month() - 1).date(0));
    this.month.month(this.month.month() - 1);
    this.monthYear = this.formatMonthYear(this.month);
    this.buildMonth(previous, this.month);
    this.calendarService.activeDay$.next(this.month.toISOString());
  }

  formatMonthYear(date: Moment): Object {
    return {month: date.locale('pl').format('MMMM'), year: date.format('YYYY')};
  }

  removeTime(date: Moment): Moment {
    return date.day(1).hour(0).minute(0).second(0).millisecond(0);
  }

  isEvent(day: Week): boolean | undefined {
    const days = [];
      this.eventsDays.forEach(eventDay => {
          days.push(moment(eventDay).format('YYYY-MM-DD'));
        }
      );
      return days.includes(moment(day.date).format('YYYY-MM-DD'));
  }

  setActiveDay(day: Moment) {
    this.activeDay = day;
    this.activeDateEmiter.emit(moment(day).format('YYYY-MM-DD'));
  }
}
