import { Component, ElementRef, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-task-short-list',
  templateUrl: './task-short-list.component.html',
  styleUrls: ['./task-short-list.component.scss']
})
export class TaskShortListComponent implements OnInit {
  @Input() tasks = [];
  @Input() height = 340;

  constructor(
    private router: Router,
    private elementRef: ElementRef
  ) { }

  ngOnInit() {
    const hostElem =  this.elementRef.nativeElement;
    hostElem.children[0].style.height = this.height + 'px';
  }

  redirectToTask(task: any) {
    this.router.navigate(['/tasks/edit', task.taskid]);
  }
}
