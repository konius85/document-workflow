import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskShortListComponent } from './task-short-list.component';
import { Router } from '@angular/router';
import { ElementRef } from '@angular/core';

class MockRouter {

}

class MockElementRef {
  constructor() {
  }
}

describe('TaskShortListComponent', () => {
  let component: TaskShortListComponent;
  let fixture: ComponentFixture<TaskShortListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TaskShortListComponent],
      providers: [
        { provide: Router, useClass: MockRouter },
        { provide: ElementRef, useClass: MockElementRef },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskShortListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
