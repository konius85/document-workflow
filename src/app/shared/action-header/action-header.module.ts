import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActionHeaderComponent } from './action-header.component';
import { CustomMaterialModule } from '../custom-material/custom-material.module';
import { SearchbarModule } from '../searchbar/searchbar.module';

@NgModule({
  declarations: [
    ActionHeaderComponent
  ],
  imports: [
    CommonModule,
    CustomMaterialModule,
    SearchbarModule,
  ],
  exports: [
    ActionHeaderComponent
  ],

})
export class ActionHeaderModule { }
