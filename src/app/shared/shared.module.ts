import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomMaterialModule } from './custom-material/custom-material.module';
import { TableModule } from './table/table.module';
import { ActionHeaderModule } from './action-header/action-header.module';
import { SearchbarModule } from './searchbar/searchbar.module';
import { DocumentFormComponent } from './forms/document-form/document-form.component';
import { IconModule } from './icon/icon.module';
import { DocumentViewerComponent } from './document-viewer/document-viewer.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TaskFormComponent } from './forms/task-form/task-form.component';
import { DocumentHistoryComponent } from './forms/document-history/document-history.component';
import { WidgetComponent } from './widget/widget.component';
import { TasksListComponent } from './tasks-list/tasks-list.component';
import { NoDataToDisplayComponent } from './no-data-to-display/no-data-to-display.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { ComboboxComponent } from './combobox/combobox.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { SnackbarComponent } from './snackbar/snackbar.component';
import { CalendarModule } from './calendar/calendar.module';
import { TaskShortListComponent } from './task-short-list/task-short-list.component';

@NgModule({
  imports: [
    CommonModule,
    CustomMaterialModule,
    TableModule,
    ReactiveFormsModule,
    ActionHeaderModule,
    SearchbarModule,
    IconModule,
    FormsModule,
    CalendarModule,
  ],
  exports: [
    CustomMaterialModule,
    TableModule,
    ActionHeaderModule,
    SearchbarModule,
    IconModule,
    DocumentFormComponent,
    TaskFormComponent,
    DocumentHistoryComponent,
    WidgetComponent,
    TasksListComponent,
    NoDataToDisplayComponent,
    UserProfileComponent,
    ComboboxComponent,
    SpinnerComponent,
    CalendarModule,
    TaskShortListComponent,

  ],
  declarations: [
    DocumentFormComponent,
    DocumentViewerComponent,
    TaskFormComponent,
    DocumentHistoryComponent,
    WidgetComponent,
    TasksListComponent,
    NoDataToDisplayComponent,
    UserProfileComponent,
    ComboboxComponent,
    SpinnerComponent,
    SnackbarComponent,
    TaskShortListComponent,
  ],
  entryComponents: [
    DocumentViewerComponent,
    SnackbarComponent
  ]
})
export class SharedModule {
}
