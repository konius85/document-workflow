import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-document-viewer',
  templateUrl: './document-viewer.component.html',
  styleUrls: ['./document-viewer.component.scss']
})
export class DocumentViewerComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private sanitizer: DomSanitizer
  ) {
  }

  documentPath;

  ngOnInit() {
    // @TODO replace current location
    const data = this.data.rowData;
    console.log('test', data)
    this.documentPath = this.sanitizer.bypassSecurityTrustResourceUrl(`http://localhost/MyApi/app/upload/${ data.path }/${ data.name }`);
  }

}
