import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { DefaultQueryService } from '../../core/default-providers/default-query.service';
import { NotificationService } from '../../core/default-providers/notyfication/notification.service';
import { EDocumentModel } from '../../core/default-providers/models/e-document.model';
import { DocumentTypeEnum } from '../../core/models/documentType.model';

@Injectable({
  providedIn: 'root'
})
export class FormService {

  constructor(
    private defaultQuery: DefaultQueryService,
    private notificationService: NotificationService,
  ) {
  }

  getDocumentTypes() {
    return this.defaultQuery.getDocumentsType();
  }

  getUsers(): Observable<any> {
    return this.defaultQuery.users$;
  }

  getStatus(type: DocumentTypeEnum) {
    return this.defaultQuery.getStatus(type);
  }

  addNotification(notification: EDocumentModel) {
    this.notificationService.addNotification(notification)
      .subscribe(_ => this.notificationService.observeWhenAddNewNotification$.next(true));
  }


}
