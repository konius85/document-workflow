import { Component, OnInit } from '@angular/core';
import { HistoryProviderService } from '../../../core/default-providers/history/history-provider.service';
import { HistoryModel } from '../../../core/default-providers/models/history.model';

@Component({
  selector: 'app-document-history',
  templateUrl: './document-history.component.html',
  styleUrls: ['./document-history.component.scss']
})
export class DocumentHistoryComponent implements OnInit {

  histories: Array<HistoryModel> = [];

  constructor(
    private historyProviderService: HistoryProviderService
  ) {
  }

  ngOnInit() {
    this.getHistory();
  }

  getHistory() {
    this.historyProviderService.getHistory().subscribe(hits => {
      this.histories = hits['hits'].map(hit => {
        const history = hit['_source'];
        history.id = hit['_id'];
        return history;
      });
    });
  }

}
