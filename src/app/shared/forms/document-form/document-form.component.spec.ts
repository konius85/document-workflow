import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentFormComponent } from './document-form.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { DocumentsService } from '../../../documents/documents.service';
import { DocumentProviderService } from '../../../core/default-providers/document/document-provider.service';
import { DefaultQueryService } from '../../../core/default-providers/default-query.service';
import { Router } from '@angular/router';
import { SnackBarService } from '../../snack-bar.service';
import { AuthenticationService } from '../../../core/auth/authentication.service';
import { FormService } from '../form.service';
import { of } from 'rxjs';

class MockDocumentsService {

}

class MockDocumentProviderService {

}

class MockDefaultQueryService {
  getDocumentsType() {
    return of([{type: 'testType'}]);
  }
}

class MockRouter {

}

class MockSnackBarService {

}

class MockAuthenticationService {

}

class MockFormService {
  getUsers() {
    return of([{userId: 1}]);
  }

  getStatus() {
    return of([{statusid: 1}]);
  }
}

describe('DocumentFormComponent', () => {
  let component: DocumentFormComponent;
  let fixture: ComponentFixture<DocumentFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [DocumentFormComponent],
      providers: [
        { provide: DocumentsService, useClass: MockDocumentsService },
        { provide: DocumentProviderService, useClass: MockDocumentProviderService },
        { provide: DefaultQueryService, useClass: MockDefaultQueryService },
        { provide: Router, useClass: MockRouter },
        { provide: SnackBarService, useClass: MockSnackBarService },
        { provide: AuthenticationService, useClass: MockAuthenticationService },
        { provide: FormService, useClass: MockFormService },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
