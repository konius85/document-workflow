import { Component, Input, OnInit } from '@angular/core';
import { UsersModel } from '../../../core/default-providers/models/users.model';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DocumentsService } from '../../../documents/documents.service';
import { DefaultQueryService } from '../../../core/default-providers/default-query.service';
import { DocumentModel } from '../../../documents/models/document.model';
import { DocumentTypeEnum } from '../../../core/models/documentType.model';
import { DocumentProviderService } from '../../../core/default-providers/document/document-provider.service';
import { AuthenticationService } from '../../../core/auth/authentication.service';
import { FormService } from '../form.service';
import { DocumentAction } from '../../../core/default-providers/models/document-action.model';
import { SnackBarService } from '../../snack-bar.service';

@Component({
  selector: 'app-document-form',
  templateUrl: './document-form.component.html',
  styleUrls: ['./document-form.component.scss']
})
export class DocumentFormComponent implements OnInit {

  @Input() documentData: DocumentModel;

  documentsTypes = [];
  users: Array<UsersModel> = [];
  status = [];
  file: File;
  fileExists = false;
  removeExistingFile = false;


  documentForm = new FormGroup({
    documentName: new FormControl('', [Validators.required]),
    type: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required]),
    file: new FormControl(''),
    user: new FormControl('', [Validators.required]),
    status: new FormControl('', [Validators.required]),
  });

  constructor(
    private documentsService: DocumentsService,
    private documentProviderService: DocumentProviderService,
    private defaultQuery: DefaultQueryService,
    private router: Router,
    private snackbar: SnackBarService,
    private authService: AuthenticationService,
    private formService: FormService,
  ) {
  }

  ngOnInit() {
    this.setValuesToDocumentForm();
    this.setFileData();
  }

  get documentName() {
    return this.documentForm.get('documentName');
  }

  get description() {
    return this.documentForm.get('description');
  }

  setValuesToDocumentForm() {
    if (this.documentData) {
      this.documentForm.controls['documentName'].setValue(this.documentData['name']);
      this.documentForm.controls['description'].setValue(this.documentData['description']);
    }

    this.getDocumentsTypes();
    this.getUsers();
    this.getStatus();
  }

  getDocumentsTypes() {
    this.defaultQuery.getDocumentsType().subscribe(data => {
      this.documentsTypes = data;
      if (this.documentData) {
        this.documentForm.controls['type'].setValue(this.documentData['documentTypeId']);
      } else {
        this.documentForm.controls['type'].setValue(data[0]['type']);
      }
    });
  }

  setFileData() {
    if (this.documentData && this.documentData['fileId']) {
      this.fileExists = true;

    }
  }

  getStatus() {
    this.formService.getStatus(DocumentTypeEnum.Document).subscribe(data => {
      this.status = data;
      if (this.documentData) {
        this.documentForm.controls['status'].setValue(this.documentData['statusId']);
      } else {
        this.documentForm.controls['status'].setValue(data[0]['statusid']);
      }
    });
  }

  getUsers() {
    this.formService.getUsers().subscribe(users => {
      this.users = users;
      if (this.documentData) {
        this.documentForm.controls['user'].setValue(this.documentData['userId']);
      } else {
        this.documentForm.controls['user'].setValue(users[0]['userId']);
      }
    });
  }

  onFile($event) {
    this.file = $event.target.files[0];
  }

  checkIsValid() {
    if (this.documentForm.valid) {
      this.onSave();
    }
  }

  onSave() {
    console.log(this.documentForm.value);
    const formData = new FormData();

    for (const data in this.documentForm.value) {
      if (data) {
        formData.append(data, this.documentForm.value[data]);
      }
    }
    if (this.file) {
      formData.append('file', this.file, this.file.name);
    }
    if (this.removeExistingFile) {
      formData.append('removeFile', 'true');
    }
    this.documentData ? this.updateDocument(formData) : this.insertDocument(formData);
  }

  insertDocument(formData) {
    this.defaultQuery.uploadFile(formData).subscribe(data => {
      this.router.navigate(['documents']);
      this.createNotification(data['docId']);
    });
  }

  createNotification(docId: number) {
    const { user } = this.documentForm.value;
    this.formService.addNotification({
      documentId: docId,
      taskId: null,
      action: this.documentData ? DocumentAction.EditDocument : DocumentAction.AddDocument,
      creationDate: new Date().toISOString(),
      fullName: '',
      userId: user,
      name: this.documentForm.value.documentName,
    });
    this.snackbar.showComponent({ message: 'Zapisano dane' });
    this.router.navigate(['documents']);

  }

  updateDocument(formData) {
    const docId = this.documentData.documentId;
    formData.append('docId', docId);
    formData.append('modificationUserId', this.authService.userId);
    this.documentProviderService.updateDocument(formData)
      .subscribe(_ => this.createNotification(docId));
  }

  removeFile() {
    this.fileExists = false;
    this.removeExistingFile = true;
  }

}
