import { TestBed } from '@angular/core/testing';

import { FormService } from './form.service';
import { DefaultQueryService } from '../../core/default-providers/default-query.service';
import { NotificationService } from '../../core/default-providers/notyfication/notification.service';

class MockDefaultQueryService {

}

class MockNotificationService {

}

describe('FormService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      { provide: DefaultQueryService, useClass: MockDefaultQueryService },
      { provide: NotificationService, useClass: MockNotificationService },
    ]
  }));

  it('should be created', () => {
    const service: FormService = TestBed.get(FormService);
    expect(service).toBeTruthy();
  });
});
