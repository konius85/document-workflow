import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskFormComponent } from './task-form.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FormService } from '../form.service';
import { TaskProviderService } from '../../../core/default-providers/task/task-provider.service';
import { AuthenticationService } from '../../../core/auth/authentication.service';
import { Router } from '@angular/router';
import { SnackBarService } from '../../snack-bar.service';
import { DocumentProviderService } from '../../../core/default-providers/document/document-provider.service';
import { of } from 'rxjs';

class MockFormService {
  getUsers() {
    return of([]);
  }
}

class MockTaskProviderService {

}

class MockAuthenticationService {

}

class MockRouter {

}

class MockSnackBarService {

}

class MockDocumentProviderService {

}

describe('TaskFormComponent', () => {
  let component: TaskFormComponent;
  let fixture: ComponentFixture<TaskFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [TaskFormComponent],
      providers: [
        { provide: FormService, useClass: MockFormService },
        { provide: TaskProviderService, useClass: MockTaskProviderService },
        { provide: AuthenticationService, useClass: MockAuthenticationService },
        { provide: Router, useClass: MockRouter },
        { provide: SnackBarService, useClass: MockSnackBarService },
        { provide: DocumentProviderService, useClass: MockDocumentProviderService },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
