import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FormService } from '../form.service';
import { TaskProviderService } from 'src/app/core/default-providers/task/task-provider.service';
import { AuthenticationService } from 'src/app/core/auth/authentication.service';
import { Router } from '@angular/router';
import { SnackBarService } from '../../snack-bar.service';
import { DocumentProviderService } from 'src/app/core/default-providers/document/document-provider.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DocumentAction } from '../../../core/default-providers/models/document-action.model';

@Component({
  selector: 'app-task-form',
  templateUrl: './task-form.component.html',
  styleUrls: ['./task-form.component.scss']
})
export class TaskFormComponent implements OnInit {
  users = [];
  @Input() taskData;
  documents$: Observable<any>;
  selectedDocument: number;

  taskForm = new FormGroup({
    taskName: new FormControl('', [Validators.required]),
    taskDescription: new FormControl('', [Validators.required]),
    user: new FormControl('', [Validators.required]),
    from: new FormControl('', [Validators.required]),
    to: new FormControl('', [Validators.required]),
    isCompleted: new FormControl('', []),
  });

  constructor(
    private formService: FormService,
    private taskProviderService: TaskProviderService,
    private authService: AuthenticationService,
    private router: Router,
    private snackBar: SnackBarService,
    private documentProviderService: DocumentProviderService,
  ) {
  }

  ngOnInit() {
    this.getUsers();
    this.setFormData();
  }

  getUsers() {
    this.formService.getUsers().subscribe(users => {
      this.users = users;
      this.taskForm.controls['user'].setValue(this.taskData ? this.taskData['userid'] : null);
    });
  }

  setFormData() {
    if (this.taskData) {
      this.taskForm.controls['taskName'].setValue(this.taskData['name']);
      this.taskForm.controls['taskDescription'].setValue(this.taskData['description']);
      this.taskForm.controls['from'].setValue(this.taskData['start'] ? new Date(this.taskData['start']) : '');
      this.taskForm.controls['to'].setValue(this.taskData['enddate'] ? new Date(this.taskData['enddate']) : '');
      this.taskForm.controls['isCompleted'].setValue(this.taskData['iscompleted']);
    }
  }

  getDocuments($event) {
    const term = $event.target.value;
    this.documents$ = this.documentProviderService
      .getDocumentByName(term)
      .pipe(
        map(documents => documents));
  }

  save() {
    const { from, to, taskName, user, taskDescription, isCompleted } = this.taskForm.value;
    const fromDate = from ? new Date(from).toLocaleString() : '';
    const toDate = to ? new Date(to).toLocaleString() : '';
    const userId = this.authService.userId;
    const data = {
      taskId: this.taskData ? this.taskData['taskid'] : null,
      taskName,
      user,
      fromDate,
      toDate,
      taskDescription,
      modUser: userId,
      isCompleted,
      taskDocument: this.selectedDocument
    };
    if (this.taskData) {
      this.taskProviderService.updateTask(data).subscribe(_ => {
        this.snackBar.showComponent({ message: 'Zmodyfikowano zadanie' });
        this.router.navigate(['/tasks']);
        this.notify(DocumentAction.EditTask);
      });
    } else {
      this.taskProviderService.addTask(data).subscribe(data => {
        this.snackBar.showComponent({ message: 'Dodano zadanie' });
        this.router.navigate(['/tasks']);
        this.notify(DocumentAction.AddTask, data[0]['taskid']);
      });
    }

    // this.router.navigate(['/tasks']);
  }

  notify(action: DocumentAction, taskId?: number) {
    const date = new Date();
    const { taskName, user } = this.taskForm.value;
    console.log('test', user)
    this.formService.addNotification({
      action: action,
      creationDate: date.toISOString(),
      taskId: taskId ? taskId : this.taskData['taskid'],
      name: taskName,
      userId: user,
    });
  }

  cancel() {
    this.router.navigate(['/tasks']);
  }

  setSelectedDocument(documentId: number) {
    this.selectedDocument = documentId;
  }


}
