import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { SnackbarComponent } from './snackbar/snackbar.component';

export enum MessageType {
  Success = 'success',
  Warning = 'warning',
}

@Injectable({
  providedIn: 'root'
})
export class SnackBarService {

  constructor(
    private snackbar: MatSnackBar,
  ) {
  }

  showComponent(data: {message: string, type?: MessageType}){
    // this.snackbar.openFromComponent(SnackbarComponent, {
    //   duration: 2000,
    //   verticalPosition: 'top',
    //   horizontalPosition: 'center',
    //   data: data ? data : '',
    // });
  }

  show(message) {
    // this.snackbar.open(message, null, {
    //   duration: 2000,
    //   verticalPosition: 'top',
    //   horizontalPosition: 'center',
    //   panelClass: 'spackbar'
    // });
  }

}
