import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.scss']
})
export class SearchbarComponent implements OnInit {

  @Output() termEmitter: EventEmitter<string> = new EventEmitter();

  ngOnInit() {
  }


  emitTerm(value: any) {
    this.termEmitter.emit(value.toLocaleLowerCase());
  }
}
