import { Component, Inject, OnInit } from '@angular/core';
// import { MAT_SNACK_BAR_DATA } from '@angular/material';
import { MessageType } from '../snack-bar.service';

@Component({
  selector: 'app-snackbar',
  templateUrl: './snackbar.component.html',
  styleUrls: ['./snackbar.component.scss']
})
export class SnackbarComponent implements OnInit {

  message: string;
  type: MessageType;
  messageType = MessageType;

  constructor() {
  }

  ngOnInit() {
    const data = {type: MessageType.Success, message: 'Test'}
    this.message = data.message;
    this.type = data.type ? data.type : MessageType.Success;
  }

}
