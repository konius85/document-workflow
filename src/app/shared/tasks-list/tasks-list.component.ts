import { Component, OnDestroy, OnInit } from '@angular/core';
import { TaskProviderService } from '../../core/default-providers/task/task-provider.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ColumnType } from '../table/models/column-type.enum';
import { TableService } from '../table/table.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MessageType, SnackBarService } from '../snack-bar.service';

@Component({
  selector: 'app-tasks-list',
  templateUrl: './tasks-list.component.html',
  styleUrls: ['./tasks-list.component.scss']
})
export class TasksListComponent implements OnInit, OnDestroy {
  tasks = [];
  unSubscribe$: Subject<boolean> = new Subject<boolean>();

  columnsConfig = [
    { label: 'Nazwa zadania', name: 'name', columnType: ColumnType.Text },
    { label: 'Opis', name: 'description', columnType: ColumnType.Text },
    { label: 'Data dodania', name: 'start', columnType: ColumnType.Date },
    { label: 'Data zakonczenia', name: 'enddate', columnType: ColumnType.Date },
    { label: 'Pracownik', name: 'employes', columnType: ColumnType.Text },
    { label: 'Zakończone', name: 'icon', columnType: ColumnType.Icon },
    { label: 'Akcje', name: 'taskid', columnType: ColumnType.Action },
  ];

  constructor(
    private taskProviderService: TaskProviderService,
    private route: ActivatedRoute,
    private tableService: TableService,
    private router: Router,
    private snackbar: SnackBarService,
  ) {
  }

  ngOnInit() {
    this.getTaskForDocuments();
    this.observerSelectedAction();
  }

  ngOnDestroy(): void {
    this.unSubscribe$.next();
    this.unSubscribe$.complete();
  }

  getTaskForDocuments() {
    const docId = this.route.snapshot.paramMap.get('id');
    this.taskProviderService.getTaskForDocument(docId)
      .pipe(takeUntil(this.unSubscribe$))
      .subscribe(tasks => {
        this.tableService.setData({
          data: tasks,
          columnsConfig: this.columnsConfig,
        });
        this.tasks = tasks;
      });
  }

  observerSelectedAction() {
    this.tableService.selectedAction$.pipe(takeUntil(this.unSubscribe$))
      .subscribe(selectedAction => {
        switch (selectedAction.action) {
          case 'redirect':
            this.router.navigate(['/tasks/edit', selectedAction.rowData]);
            break;
          case 'remove':
            this.checkIsNotCompleted(selectedAction.rowData);
            break;
        }
      });

  }

  checkIsNotCompleted(taskId) {
    const result = this.tasks.find(task => task.taskid === taskId);
    if (!result.iscompleted) {
      this.taskProviderService.removeTask(taskId.toString()).subscribe();
      this.snackbar.showComponent({ message: 'Zadanie zostało usuniete' });
    } else {
      this.snackbar.showComponent({ message: 'Nie można usunąć zakończonego zadania', type: MessageType.Warning});
    }
  }

}
