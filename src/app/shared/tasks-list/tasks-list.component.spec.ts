import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TasksListComponent } from './tasks-list.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TaskProviderService } from '../../core/default-providers/task/task-provider.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TableService } from '../table/table.service';
import { SnackBarService } from '../snack-bar.service';
import { of, ReplaySubject, Subject } from 'rxjs';
import { ActionType } from '../table/models/action-type.model';
import { DataTable } from '../table/models/data-table.model';

class MockTaskProviderService {
  getTaskForDocument() {
    return of([]);
  }
}

class MockActivatedRoute {
  snapshot = {
    paramMap: {
      get() {
        return '';
      }
    }
  };
}

class MockTableService {
  public selectedAction$: Subject<{action: ActionType, rowData: number}> = new Subject();
  public dataTable$: ReplaySubject<DataTable> = new ReplaySubject(1);

  setData() {

  }
}

class MockRouter {
}

class MockSnackBarService {

}

describe('TasksListComponent', () => {
  let component: TasksListComponent;
  let fixture: ComponentFixture<TasksListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [TasksListComponent],
      providers: [
        { provide: TaskProviderService, useClass: MockTaskProviderService },
        { provide: ActivatedRoute, useClass: MockActivatedRoute },
        { provide: TableService, useClass: MockTableService },
        { provide: Router, useClass: MockRouter },
        { provide: SnackBarService, useClass: MockSnackBarService },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TasksListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
