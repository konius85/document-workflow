import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-widget',
  templateUrl: './widget.component.html',
  styleUrls: ['./widget.component.scss']
})
export class WidgetComponent implements OnInit {

  @Output() refreshAction: EventEmitter<boolean> = new EventEmitter();
  @Input() width = 100;
  @Input() height = 100;
  @Input() title = '';
  @Input() icon = 'tasks';

  constructor() {
  }

  ngOnInit() {
  }

  emmitRefreshAction() {
    this.refreshAction.emit(true);
  }
}
