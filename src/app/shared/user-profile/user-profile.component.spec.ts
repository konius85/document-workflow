import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserProfileComponent } from './user-profile.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { UsersProviderService } from '../../core/default-providers/users/users-provider.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../../core/auth/authentication.service';
import { of } from 'rxjs';

class MockUsersProviderService {
  getUserById() {
    return of(
      { user: [{ firstname: 'test', lastname: 'test', roleid: 1, login: 'test', isdeleted: false }] }
    );
  }
}

class MockActivatedRoute {
  snapshot = {
    params: { id: 1 }
  };
}

class MockRouter {

}

class MockAuthenticationService {

}

describe('UserProfileComponent', () => {
  let component: UserProfileComponent;
  let fixture: ComponentFixture<UserProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [UserProfileComponent],
      providers: [
        { provide: UsersProviderService, useClass: MockUsersProviderService },
        { provide: ActivatedRoute, useClass: MockActivatedRoute },
        { provide: Router, useClass: MockRouter },
        { provide: AuthenticationService, useClass: MockAuthenticationService },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
