import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { UsersProviderService } from 'src/app/core/default-providers/users/users-provider.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../../core/auth/authentication.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
  roles = [];

  userForm = new FormGroup({
    login: new FormControl('', [Validators.required]),
    firstName: new FormControl('', [Validators.required]),
    lastName: new FormControl('', [Validators.required]),
    password: new FormControl('', []),
    role: new FormControl('', []),
    isDeleted: new FormControl('', []),

  });

  constructor(
    private usersProviderService: UsersProviderService,
    private route: ActivatedRoute,
    private router: Router,
    private auth: AuthenticationService,
  ) {
  }

  ngOnInit() {
    this.getUserBiId();
  }

  getUserBiId() {
    let id = this.route.snapshot.params['id'];
    if (!id) {
      id = this.auth.userId;
    }
    this.usersProviderService.getUserById(id).subscribe(d => this.setFormValue(d));
  }

  setFormValue(data) {
    this.userForm.controls['firstName'].setValue(data.user[0]['firstname']);
    this.userForm.controls['lastName'].setValue(data.user[0]['lastname']);
    this.userForm.controls['role'].setValue(data.user[0]['roleid']);
    this.userForm.controls['login'].setValue(data.user[0]['login']);
    this.userForm.controls['isDeleted'].setValue(data.user[0]['isdeleted']);
    this.roles = data.roles;
  }

  onSave() {
    const data = this.userForm.value;
    data.usrId = Number(this.route.snapshot.params['id']);
    this.usersProviderService.updateUser(data).subscribe(data1 => console.log('test', data1));
  }

  redirect() {
    this.router.navigate(['/tasks']);
  }
}
