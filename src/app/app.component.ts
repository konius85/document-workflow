import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from './core/auth/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'DocumentWorkflow';
  isLogged = null;

  constructor(
    private authenticationService: AuthenticationService,
  ) { }

  ngOnInit(): void {
    this.authenticationService.currentUserSubject.subscribe(data => {
      this.isLogged = !!data;
    });
  }


}
