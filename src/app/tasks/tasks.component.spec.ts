import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TasksComponent } from './tasks.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TaskService } from './task.service';
import { TableService } from '../shared/table/table.service';
import { Router } from '@angular/router';
import { TaskProviderService } from '../core/default-providers/task/task-provider.service';
import { SnackBarService } from '../shared/snack-bar.service';
import { of, ReplaySubject, Subject } from 'rxjs';
import { ActionType } from '../shared/table/models/action-type.model';
import { DataTable } from '../shared/table/models/data-table.model';

class MockTaskService {
  getTask() {
    return of([]);
  }
  setData() {

  }
}

class MockTableService {
  public selectedAction$: Subject<{ action: ActionType, rowData: number }> = new Subject();
  public dataTable$: ReplaySubject<DataTable> = new ReplaySubject(1);
}

class MockRouter {

}

class MockTaskProviderService {
  getTaskById(){
    return of([]);
  }
}

class MockSnackBarService {

}

describe('TasksComponent', () => {
  let component: TasksComponent;
  let fixture: ComponentFixture<TasksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [TasksComponent],
      providers: [
        { provide: TaskService, useClass: MockTaskService },
        { provide: TableService, useClass: MockTableService },
        { provide: Router, useClass: MockRouter },
        { provide: TaskProviderService, useClass: MockTaskProviderService },
        { provide: SnackBarService, useClass: MockSnackBarService },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TasksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
