import { Component, OnDestroy, OnInit } from '@angular/core';
import { TaskService } from './task.service';
import { Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';
import { TableService } from '../shared/table/table.service';
import { ActionType } from '../shared/table/models/action-type.model';
import { Router } from '@angular/router';
import { TaskProviderService } from '../core/default-providers/task/task-provider.service';
import { MessageType, SnackBarService } from '../shared/snack-bar.service';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit, OnDestroy {

  unSubscribe: Subject<boolean> = new Subject();
  private tasks = [];
  loading = true;

  constructor(
    private taskService: TaskService,
    private tableService: TableService,
    private router: Router,
    private taskProviderService: TaskProviderService,
    private snackbar: SnackBarService,
  ) {
  }

  ngOnInit() {
    this.getTasks();
    this.observerSelectedAction();
  }

  ngOnDestroy() {
    this.unSubscribe.next(true);
    this.unSubscribe.complete();
  }

  getTasks() {
    this.taskService.getTask()
      .pipe(takeUntil(this.unSubscribe),
        tap(_ => this.loading = false))
      .subscribe(tasks => {
        this.taskService.setData(tasks['tasks']);
        this.tasks = tasks['tasks'];
      });
  }

  onSearch($event: string) {
    this.taskProviderService.getTasksByName($event.toLowerCase()).pipe(takeUntil(this.unSubscribe))
      .subscribe(tasks => {
        this.taskService.setData(tasks['tasks']);
        this.tasks = tasks['tasks'];
      });

  }

  observerSelectedAction() {
    this.tableService.selectedAction$.pipe(
      takeUntil(this.unSubscribe))
      .subscribe(selectedAction => {
        switch (selectedAction.action) {
          case ActionType.Redirect:
            this.router.navigate(['/tasks/edit', selectedAction.rowData]);
            break;
          case ActionType.Remove:
            this.checkIsNotCompleted(selectedAction.rowData);
            break;
        }
      });
  }

  checkIsNotCompleted(taskId) {
    const result = this.tasks.find(task => task.taskid === taskId);
    if (!result.iscompleted) {
      this.taskProviderService.removeTask(taskId.toString()).subscribe();
      this.snackbar.showComponent({ message: 'Zadanie zostało usuniete' });
    } else {
      this.snackbar.showComponent({ message: 'Nie można usunąć zakończonego zadania', type: MessageType.Warning});
    }
  }

  redirectToTaskForm() {
    this.router.navigate(['/tasks/add']);
  }


}
