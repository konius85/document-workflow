import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TasksComponent} from './tasks.component';
import {TasksRoutingModule} from './tasks-routing.module';
import { TableModule } from '../shared/table/table.module';
import { SharedModule } from '../shared/shared.module';
import { EditTaskComponent } from './edit-task/edit-task.component';
import { AddTaskComponent } from './add-task/add-task.component';

@NgModule({
  declarations: [
    TasksComponent,
    EditTaskComponent,
    AddTaskComponent
  ],
  imports: [
    CommonModule,
    TasksRoutingModule,
    TableModule,
    SharedModule,
  ],
  exports: [TasksComponent]
})
export class TasksModule { }
