import { TestBed } from '@angular/core/testing';

import { TaskService } from './task.service';
import { TableService } from '../shared/table/table.service';
import { TaskProviderService } from '../core/default-providers/task/task-provider.service';

class MockTableService {

}

class MockTaskProviderService {

}

describe('TaskService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      { provide: TableService, useClass: MockTableService },
      { provide: TaskProviderService, useClass: MockTaskProviderService },
    ]
  }));

  it('should be created', () => {
    const service: TaskService = TestBed.get(TaskService);
    expect(service).toBeTruthy();
  });
});
