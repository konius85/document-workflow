import { Injectable } from '@angular/core';
import { ColumnType } from '../shared/table/models/column-type.enum';
import { TableService } from '../shared/table/table.service';
import { TaskProviderService } from '../core/default-providers/task/task-provider.service';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  columnsConfig = [
    { label: 'Nazwa zadania', name: 'name', columnType: ColumnType.Text },
    { label: 'Opis', name: 'description', columnType: ColumnType.Text },
    { label: 'Data rozpoczęcia', name: 'start', columnType: ColumnType.Date },
    { label: 'Data zakończenia', name: 'enddate', columnType: ColumnType.Date },
    { label: 'Zakończone', name: 'icon', columnType: ColumnType.Icon },
    { label: 'Akcje', name: 'taskid', columnType: ColumnType.Action },
  ];

  constructor(
    private tableService: TableService,
    private taskProviderService: TaskProviderService,
  ) {
  }

  getTask() {
    return this.taskProviderService.getTasks();
  }

  public setData(data: Array<any>) {
    this.tableService.setData({
      columnsConfig: this.columnsConfig,
      data: data
    });
  }

}
