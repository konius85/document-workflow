import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TaskProviderService } from '../../core/default-providers/task/task-provider.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-edit-task',
  templateUrl: './edit-task.component.html',
  styleUrls: ['./edit-task.component.scss']
})
export class EditTaskComponent implements OnInit {

  constructor(
    private taskProviderService: TaskProviderService,
    private activatedRoute: ActivatedRoute,
  ) { }

  taskData;

  ngOnInit() {
    this.getTaskData();
  }

  getTaskData() {
    this.activatedRoute.params.subscribe(routerParam => {
      this.taskProviderService.getTaskById(routerParam.id)
        .pipe(map(task => task['task']))
        .subscribe(data => this.taskData = data[0]);
    });
  }

}
