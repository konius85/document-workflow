import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTaskComponent } from './edit-task.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TaskProviderService } from '../../core/default-providers/task/task-provider.service';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

class MockTaskProviderService {
  getTaskById(){
    return of({task: {taskId: 'testTask'}});
  }
}

class MockActivatedRoute {
  params = of({ id: 3 })
}

describe('EditTaskComponent', () => {
  let component: EditTaskComponent;
  let fixture: ComponentFixture<EditTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [EditTaskComponent],
      providers: [
        { provide: TaskProviderService, useClass: MockTaskProviderService },
        { provide: ActivatedRoute, useClass: MockActivatedRoute },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
